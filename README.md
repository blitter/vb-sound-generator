VB Sound Generator
==================

This tool provides a way to play around with the parameters of the Virtual Boy's sound chip in real time. I took DogP's VB Sound Generator code here [http://www.planetvb.com/modules/tech/?sec=tools&pid=vsuemu](http://www.planetvb.com/modules/tech/?sec=tools&pid=vsuemu) and created a GUI wrapper around the VSU emulator part of it, making it a sort of VSU sandbox. Available under the 3-clause BSD license.

How to Use
----------

- The sliders on the left side of the window control the waveform data. Select a waveform bank using the tabs above. Each slider represents one byte. Alternatively, presets are available through the buttons underneath the tabs.
- The tabbed interface on the right side of the window control each channel's settings and volume. Register values are updated in real time at the bottom of the panel, and vice-versa.
- Check the "Enabled" checkbox in the channel panel to enable that channel and thereby generate audio.

Build Notes
-----------

The GUI portion was written in Objective-C++/Cocoa. The VSU and audio engine parts are written in C++. The tool uses PortAudio for playback and is built for OS X 10.6 using XCode 4.

Have fun.

Keith Kaisershot

3-14-17
