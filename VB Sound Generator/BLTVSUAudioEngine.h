//
//  BLTVSUAudioEngine.h
//  VB Sound Generator
//
//  Created by Keith Kaisershot on 3/8/14.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

struct vsu_regs;

@interface BLTVSUAudioEngine : NSObject

- (BOOL)start:(vsu_regs&)vsu;
- (void)update:(vsu_regs&)vsu;
- (void)stop;

@end
