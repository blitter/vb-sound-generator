//
//  BLTAppDelegate.h
//  VB Sound Generator
//
//  Created by Keith Kaisershot on 2/20/14.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface BLTAppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@property (assign) IBOutlet NSButton *wave1SineButton;
@property (assign) IBOutlet NSButton *wave1SquareButton;
@property (assign) IBOutlet NSButton *wave1TriangleButton;
@property (assign) IBOutlet NSButton *wave1SawtoothButton;

@property (assign) IBOutlet NSSlider *wave1Word1Slider;
@property (assign) IBOutlet NSSlider *wave1Word2Slider;
@property (assign) IBOutlet NSSlider *wave1Word3Slider;
@property (assign) IBOutlet NSSlider *wave1Word4Slider;
@property (assign) IBOutlet NSSlider *wave1Word5Slider;
@property (assign) IBOutlet NSSlider *wave1Word6Slider;
@property (assign) IBOutlet NSSlider *wave1Word7Slider;
@property (assign) IBOutlet NSSlider *wave1Word8Slider;
@property (assign) IBOutlet NSSlider *wave1Word9Slider;
@property (assign) IBOutlet NSSlider *wave1Word10Slider;
@property (assign) IBOutlet NSSlider *wave1Word11Slider;
@property (assign) IBOutlet NSSlider *wave1Word12Slider;
@property (assign) IBOutlet NSSlider *wave1Word13Slider;
@property (assign) IBOutlet NSSlider *wave1Word14Slider;
@property (assign) IBOutlet NSSlider *wave1Word15Slider;
@property (assign) IBOutlet NSSlider *wave1Word16Slider;
@property (assign) IBOutlet NSSlider *wave1Word17Slider;
@property (assign) IBOutlet NSSlider *wave1Word18Slider;
@property (assign) IBOutlet NSSlider *wave1Word19Slider;
@property (assign) IBOutlet NSSlider *wave1Word20Slider;
@property (assign) IBOutlet NSSlider *wave1Word21Slider;
@property (assign) IBOutlet NSSlider *wave1Word22Slider;
@property (assign) IBOutlet NSSlider *wave1Word23Slider;
@property (assign) IBOutlet NSSlider *wave1Word24Slider;
@property (assign) IBOutlet NSSlider *wave1Word25Slider;
@property (assign) IBOutlet NSSlider *wave1Word26Slider;
@property (assign) IBOutlet NSSlider *wave1Word27Slider;
@property (assign) IBOutlet NSSlider *wave1Word28Slider;
@property (assign) IBOutlet NSSlider *wave1Word29Slider;
@property (assign) IBOutlet NSSlider *wave1Word30Slider;
@property (assign) IBOutlet NSSlider *wave1Word31Slider;
@property (assign) IBOutlet NSSlider *wave1Word32Slider;

@property (assign) IBOutlet NSStepper *wave1Word1Stepper;
@property (assign) IBOutlet NSStepper *wave1Word2Stepper;
@property (assign) IBOutlet NSStepper *wave1Word3Stepper;
@property (assign) IBOutlet NSStepper *wave1Word4Stepper;
@property (assign) IBOutlet NSStepper *wave1Word5Stepper;
@property (assign) IBOutlet NSStepper *wave1Word6Stepper;
@property (assign) IBOutlet NSStepper *wave1Word7Stepper;
@property (assign) IBOutlet NSStepper *wave1Word8Stepper;
@property (assign) IBOutlet NSStepper *wave1Word9Stepper;
@property (assign) IBOutlet NSStepper *wave1Word10Stepper;
@property (assign) IBOutlet NSStepper *wave1Word11Stepper;
@property (assign) IBOutlet NSStepper *wave1Word12Stepper;
@property (assign) IBOutlet NSStepper *wave1Word13Stepper;
@property (assign) IBOutlet NSStepper *wave1Word14Stepper;
@property (assign) IBOutlet NSStepper *wave1Word15Stepper;
@property (assign) IBOutlet NSStepper *wave1Word16Stepper;
@property (assign) IBOutlet NSStepper *wave1Word17Stepper;
@property (assign) IBOutlet NSStepper *wave1Word18Stepper;
@property (assign) IBOutlet NSStepper *wave1Word19Stepper;
@property (assign) IBOutlet NSStepper *wave1Word20Stepper;
@property (assign) IBOutlet NSStepper *wave1Word21Stepper;
@property (assign) IBOutlet NSStepper *wave1Word22Stepper;
@property (assign) IBOutlet NSStepper *wave1Word23Stepper;
@property (assign) IBOutlet NSStepper *wave1Word24Stepper;
@property (assign) IBOutlet NSStepper *wave1Word25Stepper;
@property (assign) IBOutlet NSStepper *wave1Word26Stepper;
@property (assign) IBOutlet NSStepper *wave1Word27Stepper;
@property (assign) IBOutlet NSStepper *wave1Word28Stepper;
@property (assign) IBOutlet NSStepper *wave1Word29Stepper;
@property (assign) IBOutlet NSStepper *wave1Word30Stepper;
@property (assign) IBOutlet NSStepper *wave1Word31Stepper;
@property (assign) IBOutlet NSStepper *wave1Word32Stepper;

@property (assign) IBOutlet NSButton *wave2SineButton;
@property (assign) IBOutlet NSButton *wave2SquareButton;
@property (assign) IBOutlet NSButton *wave2TriangleButton;
@property (assign) IBOutlet NSButton *wave2SawtoothButton;

@property (assign) IBOutlet NSSlider *wave2Word1Slider;
@property (assign) IBOutlet NSSlider *wave2Word2Slider;
@property (assign) IBOutlet NSSlider *wave2Word3Slider;
@property (assign) IBOutlet NSSlider *wave2Word4Slider;
@property (assign) IBOutlet NSSlider *wave2Word5Slider;
@property (assign) IBOutlet NSSlider *wave2Word6Slider;
@property (assign) IBOutlet NSSlider *wave2Word7Slider;
@property (assign) IBOutlet NSSlider *wave2Word8Slider;
@property (assign) IBOutlet NSSlider *wave2Word9Slider;
@property (assign) IBOutlet NSSlider *wave2Word10Slider;
@property (assign) IBOutlet NSSlider *wave2Word11Slider;
@property (assign) IBOutlet NSSlider *wave2Word12Slider;
@property (assign) IBOutlet NSSlider *wave2Word13Slider;
@property (assign) IBOutlet NSSlider *wave2Word14Slider;
@property (assign) IBOutlet NSSlider *wave2Word15Slider;
@property (assign) IBOutlet NSSlider *wave2Word16Slider;
@property (assign) IBOutlet NSSlider *wave2Word17Slider;
@property (assign) IBOutlet NSSlider *wave2Word18Slider;
@property (assign) IBOutlet NSSlider *wave2Word19Slider;
@property (assign) IBOutlet NSSlider *wave2Word20Slider;
@property (assign) IBOutlet NSSlider *wave2Word21Slider;
@property (assign) IBOutlet NSSlider *wave2Word22Slider;
@property (assign) IBOutlet NSSlider *wave2Word23Slider;
@property (assign) IBOutlet NSSlider *wave2Word24Slider;
@property (assign) IBOutlet NSSlider *wave2Word25Slider;
@property (assign) IBOutlet NSSlider *wave2Word26Slider;
@property (assign) IBOutlet NSSlider *wave2Word27Slider;
@property (assign) IBOutlet NSSlider *wave2Word28Slider;
@property (assign) IBOutlet NSSlider *wave2Word29Slider;
@property (assign) IBOutlet NSSlider *wave2Word30Slider;
@property (assign) IBOutlet NSSlider *wave2Word31Slider;
@property (assign) IBOutlet NSSlider *wave2Word32Slider;

@property (assign) IBOutlet NSStepper *wave2Word1Stepper;
@property (assign) IBOutlet NSStepper *wave2Word2Stepper;
@property (assign) IBOutlet NSStepper *wave2Word3Stepper;
@property (assign) IBOutlet NSStepper *wave2Word4Stepper;
@property (assign) IBOutlet NSStepper *wave2Word5Stepper;
@property (assign) IBOutlet NSStepper *wave2Word6Stepper;
@property (assign) IBOutlet NSStepper *wave2Word7Stepper;
@property (assign) IBOutlet NSStepper *wave2Word8Stepper;
@property (assign) IBOutlet NSStepper *wave2Word9Stepper;
@property (assign) IBOutlet NSStepper *wave2Word10Stepper;
@property (assign) IBOutlet NSStepper *wave2Word11Stepper;
@property (assign) IBOutlet NSStepper *wave2Word12Stepper;
@property (assign) IBOutlet NSStepper *wave2Word13Stepper;
@property (assign) IBOutlet NSStepper *wave2Word14Stepper;
@property (assign) IBOutlet NSStepper *wave2Word15Stepper;
@property (assign) IBOutlet NSStepper *wave2Word16Stepper;
@property (assign) IBOutlet NSStepper *wave2Word17Stepper;
@property (assign) IBOutlet NSStepper *wave2Word18Stepper;
@property (assign) IBOutlet NSStepper *wave2Word19Stepper;
@property (assign) IBOutlet NSStepper *wave2Word20Stepper;
@property (assign) IBOutlet NSStepper *wave2Word21Stepper;
@property (assign) IBOutlet NSStepper *wave2Word22Stepper;
@property (assign) IBOutlet NSStepper *wave2Word23Stepper;
@property (assign) IBOutlet NSStepper *wave2Word24Stepper;
@property (assign) IBOutlet NSStepper *wave2Word25Stepper;
@property (assign) IBOutlet NSStepper *wave2Word26Stepper;
@property (assign) IBOutlet NSStepper *wave2Word27Stepper;
@property (assign) IBOutlet NSStepper *wave2Word28Stepper;
@property (assign) IBOutlet NSStepper *wave2Word29Stepper;
@property (assign) IBOutlet NSStepper *wave2Word30Stepper;
@property (assign) IBOutlet NSStepper *wave2Word31Stepper;
@property (assign) IBOutlet NSStepper *wave2Word32Stepper;

@property (assign) IBOutlet NSButton *wave3SineButton;
@property (assign) IBOutlet NSButton *wave3SquareButton;
@property (assign) IBOutlet NSButton *wave3TriangleButton;
@property (assign) IBOutlet NSButton *wave3SawtoothButton;

@property (assign) IBOutlet NSSlider *wave3Word1Slider;
@property (assign) IBOutlet NSSlider *wave3Word2Slider;
@property (assign) IBOutlet NSSlider *wave3Word3Slider;
@property (assign) IBOutlet NSSlider *wave3Word4Slider;
@property (assign) IBOutlet NSSlider *wave3Word5Slider;
@property (assign) IBOutlet NSSlider *wave3Word6Slider;
@property (assign) IBOutlet NSSlider *wave3Word7Slider;
@property (assign) IBOutlet NSSlider *wave3Word8Slider;
@property (assign) IBOutlet NSSlider *wave3Word9Slider;
@property (assign) IBOutlet NSSlider *wave3Word10Slider;
@property (assign) IBOutlet NSSlider *wave3Word11Slider;
@property (assign) IBOutlet NSSlider *wave3Word12Slider;
@property (assign) IBOutlet NSSlider *wave3Word13Slider;
@property (assign) IBOutlet NSSlider *wave3Word14Slider;
@property (assign) IBOutlet NSSlider *wave3Word15Slider;
@property (assign) IBOutlet NSSlider *wave3Word16Slider;
@property (assign) IBOutlet NSSlider *wave3Word17Slider;
@property (assign) IBOutlet NSSlider *wave3Word18Slider;
@property (assign) IBOutlet NSSlider *wave3Word19Slider;
@property (assign) IBOutlet NSSlider *wave3Word20Slider;
@property (assign) IBOutlet NSSlider *wave3Word21Slider;
@property (assign) IBOutlet NSSlider *wave3Word22Slider;
@property (assign) IBOutlet NSSlider *wave3Word23Slider;
@property (assign) IBOutlet NSSlider *wave3Word24Slider;
@property (assign) IBOutlet NSSlider *wave3Word25Slider;
@property (assign) IBOutlet NSSlider *wave3Word26Slider;
@property (assign) IBOutlet NSSlider *wave3Word27Slider;
@property (assign) IBOutlet NSSlider *wave3Word28Slider;
@property (assign) IBOutlet NSSlider *wave3Word29Slider;
@property (assign) IBOutlet NSSlider *wave3Word30Slider;
@property (assign) IBOutlet NSSlider *wave3Word31Slider;
@property (assign) IBOutlet NSSlider *wave3Word32Slider;

@property (assign) IBOutlet NSStepper *wave3Word1Stepper;
@property (assign) IBOutlet NSStepper *wave3Word2Stepper;
@property (assign) IBOutlet NSStepper *wave3Word3Stepper;
@property (assign) IBOutlet NSStepper *wave3Word4Stepper;
@property (assign) IBOutlet NSStepper *wave3Word5Stepper;
@property (assign) IBOutlet NSStepper *wave3Word6Stepper;
@property (assign) IBOutlet NSStepper *wave3Word7Stepper;
@property (assign) IBOutlet NSStepper *wave3Word8Stepper;
@property (assign) IBOutlet NSStepper *wave3Word9Stepper;
@property (assign) IBOutlet NSStepper *wave3Word10Stepper;
@property (assign) IBOutlet NSStepper *wave3Word11Stepper;
@property (assign) IBOutlet NSStepper *wave3Word12Stepper;
@property (assign) IBOutlet NSStepper *wave3Word13Stepper;
@property (assign) IBOutlet NSStepper *wave3Word14Stepper;
@property (assign) IBOutlet NSStepper *wave3Word15Stepper;
@property (assign) IBOutlet NSStepper *wave3Word16Stepper;
@property (assign) IBOutlet NSStepper *wave3Word17Stepper;
@property (assign) IBOutlet NSStepper *wave3Word18Stepper;
@property (assign) IBOutlet NSStepper *wave3Word19Stepper;
@property (assign) IBOutlet NSStepper *wave3Word20Stepper;
@property (assign) IBOutlet NSStepper *wave3Word21Stepper;
@property (assign) IBOutlet NSStepper *wave3Word22Stepper;
@property (assign) IBOutlet NSStepper *wave3Word23Stepper;
@property (assign) IBOutlet NSStepper *wave3Word24Stepper;
@property (assign) IBOutlet NSStepper *wave3Word25Stepper;
@property (assign) IBOutlet NSStepper *wave3Word26Stepper;
@property (assign) IBOutlet NSStepper *wave3Word27Stepper;
@property (assign) IBOutlet NSStepper *wave3Word28Stepper;
@property (assign) IBOutlet NSStepper *wave3Word29Stepper;
@property (assign) IBOutlet NSStepper *wave3Word30Stepper;
@property (assign) IBOutlet NSStepper *wave3Word31Stepper;
@property (assign) IBOutlet NSStepper *wave3Word32Stepper;

@property (assign) IBOutlet NSButton *wave4SineButton;
@property (assign) IBOutlet NSButton *wave4SquareButton;
@property (assign) IBOutlet NSButton *wave4TriangleButton;
@property (assign) IBOutlet NSButton *wave4SawtoothButton;

@property (assign) IBOutlet NSSlider *wave4Word1Slider;
@property (assign) IBOutlet NSSlider *wave4Word2Slider;
@property (assign) IBOutlet NSSlider *wave4Word3Slider;
@property (assign) IBOutlet NSSlider *wave4Word4Slider;
@property (assign) IBOutlet NSSlider *wave4Word5Slider;
@property (assign) IBOutlet NSSlider *wave4Word6Slider;
@property (assign) IBOutlet NSSlider *wave4Word7Slider;
@property (assign) IBOutlet NSSlider *wave4Word8Slider;
@property (assign) IBOutlet NSSlider *wave4Word9Slider;
@property (assign) IBOutlet NSSlider *wave4Word10Slider;
@property (assign) IBOutlet NSSlider *wave4Word11Slider;
@property (assign) IBOutlet NSSlider *wave4Word12Slider;
@property (assign) IBOutlet NSSlider *wave4Word13Slider;
@property (assign) IBOutlet NSSlider *wave4Word14Slider;
@property (assign) IBOutlet NSSlider *wave4Word15Slider;
@property (assign) IBOutlet NSSlider *wave4Word16Slider;
@property (assign) IBOutlet NSSlider *wave4Word17Slider;
@property (assign) IBOutlet NSSlider *wave4Word18Slider;
@property (assign) IBOutlet NSSlider *wave4Word19Slider;
@property (assign) IBOutlet NSSlider *wave4Word20Slider;
@property (assign) IBOutlet NSSlider *wave4Word21Slider;
@property (assign) IBOutlet NSSlider *wave4Word22Slider;
@property (assign) IBOutlet NSSlider *wave4Word23Slider;
@property (assign) IBOutlet NSSlider *wave4Word24Slider;
@property (assign) IBOutlet NSSlider *wave4Word25Slider;
@property (assign) IBOutlet NSSlider *wave4Word26Slider;
@property (assign) IBOutlet NSSlider *wave4Word27Slider;
@property (assign) IBOutlet NSSlider *wave4Word28Slider;
@property (assign) IBOutlet NSSlider *wave4Word29Slider;
@property (assign) IBOutlet NSSlider *wave4Word30Slider;
@property (assign) IBOutlet NSSlider *wave4Word31Slider;
@property (assign) IBOutlet NSSlider *wave4Word32Slider;

@property (assign) IBOutlet NSStepper *wave4Word1Stepper;
@property (assign) IBOutlet NSStepper *wave4Word2Stepper;
@property (assign) IBOutlet NSStepper *wave4Word3Stepper;
@property (assign) IBOutlet NSStepper *wave4Word4Stepper;
@property (assign) IBOutlet NSStepper *wave4Word5Stepper;
@property (assign) IBOutlet NSStepper *wave4Word6Stepper;
@property (assign) IBOutlet NSStepper *wave4Word7Stepper;
@property (assign) IBOutlet NSStepper *wave4Word8Stepper;
@property (assign) IBOutlet NSStepper *wave4Word9Stepper;
@property (assign) IBOutlet NSStepper *wave4Word10Stepper;
@property (assign) IBOutlet NSStepper *wave4Word11Stepper;
@property (assign) IBOutlet NSStepper *wave4Word12Stepper;
@property (assign) IBOutlet NSStepper *wave4Word13Stepper;
@property (assign) IBOutlet NSStepper *wave4Word14Stepper;
@property (assign) IBOutlet NSStepper *wave4Word15Stepper;
@property (assign) IBOutlet NSStepper *wave4Word16Stepper;
@property (assign) IBOutlet NSStepper *wave4Word17Stepper;
@property (assign) IBOutlet NSStepper *wave4Word18Stepper;
@property (assign) IBOutlet NSStepper *wave4Word19Stepper;
@property (assign) IBOutlet NSStepper *wave4Word20Stepper;
@property (assign) IBOutlet NSStepper *wave4Word21Stepper;
@property (assign) IBOutlet NSStepper *wave4Word22Stepper;
@property (assign) IBOutlet NSStepper *wave4Word23Stepper;
@property (assign) IBOutlet NSStepper *wave4Word24Stepper;
@property (assign) IBOutlet NSStepper *wave4Word25Stepper;
@property (assign) IBOutlet NSStepper *wave4Word26Stepper;
@property (assign) IBOutlet NSStepper *wave4Word27Stepper;
@property (assign) IBOutlet NSStepper *wave4Word28Stepper;
@property (assign) IBOutlet NSStepper *wave4Word29Stepper;
@property (assign) IBOutlet NSStepper *wave4Word30Stepper;
@property (assign) IBOutlet NSStepper *wave4Word31Stepper;
@property (assign) IBOutlet NSStepper *wave4Word32Stepper;

@property (assign) IBOutlet NSButton *wave5SineButton;
@property (assign) IBOutlet NSButton *wave5SquareButton;
@property (assign) IBOutlet NSButton *wave5TriangleButton;
@property (assign) IBOutlet NSButton *wave5SawtoothButton;

@property (assign) IBOutlet NSSlider *wave5Word1Slider;
@property (assign) IBOutlet NSSlider *wave5Word2Slider;
@property (assign) IBOutlet NSSlider *wave5Word3Slider;
@property (assign) IBOutlet NSSlider *wave5Word4Slider;
@property (assign) IBOutlet NSSlider *wave5Word5Slider;
@property (assign) IBOutlet NSSlider *wave5Word6Slider;
@property (assign) IBOutlet NSSlider *wave5Word7Slider;
@property (assign) IBOutlet NSSlider *wave5Word8Slider;
@property (assign) IBOutlet NSSlider *wave5Word9Slider;
@property (assign) IBOutlet NSSlider *wave5Word10Slider;
@property (assign) IBOutlet NSSlider *wave5Word11Slider;
@property (assign) IBOutlet NSSlider *wave5Word12Slider;
@property (assign) IBOutlet NSSlider *wave5Word13Slider;
@property (assign) IBOutlet NSSlider *wave5Word14Slider;
@property (assign) IBOutlet NSSlider *wave5Word15Slider;
@property (assign) IBOutlet NSSlider *wave5Word16Slider;
@property (assign) IBOutlet NSSlider *wave5Word17Slider;
@property (assign) IBOutlet NSSlider *wave5Word18Slider;
@property (assign) IBOutlet NSSlider *wave5Word19Slider;
@property (assign) IBOutlet NSSlider *wave5Word20Slider;
@property (assign) IBOutlet NSSlider *wave5Word21Slider;
@property (assign) IBOutlet NSSlider *wave5Word22Slider;
@property (assign) IBOutlet NSSlider *wave5Word23Slider;
@property (assign) IBOutlet NSSlider *wave5Word24Slider;
@property (assign) IBOutlet NSSlider *wave5Word25Slider;
@property (assign) IBOutlet NSSlider *wave5Word26Slider;
@property (assign) IBOutlet NSSlider *wave5Word27Slider;
@property (assign) IBOutlet NSSlider *wave5Word28Slider;
@property (assign) IBOutlet NSSlider *wave5Word29Slider;
@property (assign) IBOutlet NSSlider *wave5Word30Slider;
@property (assign) IBOutlet NSSlider *wave5Word31Slider;
@property (assign) IBOutlet NSSlider *wave5Word32Slider;

@property (assign) IBOutlet NSStepper *wave5Word1Stepper;
@property (assign) IBOutlet NSStepper *wave5Word2Stepper;
@property (assign) IBOutlet NSStepper *wave5Word3Stepper;
@property (assign) IBOutlet NSStepper *wave5Word4Stepper;
@property (assign) IBOutlet NSStepper *wave5Word5Stepper;
@property (assign) IBOutlet NSStepper *wave5Word6Stepper;
@property (assign) IBOutlet NSStepper *wave5Word7Stepper;
@property (assign) IBOutlet NSStepper *wave5Word8Stepper;
@property (assign) IBOutlet NSStepper *wave5Word9Stepper;
@property (assign) IBOutlet NSStepper *wave5Word10Stepper;
@property (assign) IBOutlet NSStepper *wave5Word11Stepper;
@property (assign) IBOutlet NSStepper *wave5Word12Stepper;
@property (assign) IBOutlet NSStepper *wave5Word13Stepper;
@property (assign) IBOutlet NSStepper *wave5Word14Stepper;
@property (assign) IBOutlet NSStepper *wave5Word15Stepper;
@property (assign) IBOutlet NSStepper *wave5Word16Stepper;
@property (assign) IBOutlet NSStepper *wave5Word17Stepper;
@property (assign) IBOutlet NSStepper *wave5Word18Stepper;
@property (assign) IBOutlet NSStepper *wave5Word19Stepper;
@property (assign) IBOutlet NSStepper *wave5Word20Stepper;
@property (assign) IBOutlet NSStepper *wave5Word21Stepper;
@property (assign) IBOutlet NSStepper *wave5Word22Stepper;
@property (assign) IBOutlet NSStepper *wave5Word23Stepper;
@property (assign) IBOutlet NSStepper *wave5Word24Stepper;
@property (assign) IBOutlet NSStepper *wave5Word25Stepper;
@property (assign) IBOutlet NSStepper *wave5Word26Stepper;
@property (assign) IBOutlet NSStepper *wave5Word27Stepper;
@property (assign) IBOutlet NSStepper *wave5Word28Stepper;
@property (assign) IBOutlet NSStepper *wave5Word29Stepper;
@property (assign) IBOutlet NSStepper *wave5Word30Stepper;
@property (assign) IBOutlet NSStepper *wave5Word31Stepper;
@property (assign) IBOutlet NSStepper *wave5Word32Stepper;

@property (assign) IBOutlet NSButton *modSineButton;
@property (assign) IBOutlet NSButton *modSquareButton;
@property (assign) IBOutlet NSButton *modTriangleButton;
@property (assign) IBOutlet NSButton *modSawtoothButton;

@property (assign) IBOutlet NSSlider *modWord1Slider;
@property (assign) IBOutlet NSSlider *modWord2Slider;
@property (assign) IBOutlet NSSlider *modWord3Slider;
@property (assign) IBOutlet NSSlider *modWord4Slider;
@property (assign) IBOutlet NSSlider *modWord5Slider;
@property (assign) IBOutlet NSSlider *modWord6Slider;
@property (assign) IBOutlet NSSlider *modWord7Slider;
@property (assign) IBOutlet NSSlider *modWord8Slider;
@property (assign) IBOutlet NSSlider *modWord9Slider;
@property (assign) IBOutlet NSSlider *modWord10Slider;
@property (assign) IBOutlet NSSlider *modWord11Slider;
@property (assign) IBOutlet NSSlider *modWord12Slider;
@property (assign) IBOutlet NSSlider *modWord13Slider;
@property (assign) IBOutlet NSSlider *modWord14Slider;
@property (assign) IBOutlet NSSlider *modWord15Slider;
@property (assign) IBOutlet NSSlider *modWord16Slider;
@property (assign) IBOutlet NSSlider *modWord17Slider;
@property (assign) IBOutlet NSSlider *modWord18Slider;
@property (assign) IBOutlet NSSlider *modWord19Slider;
@property (assign) IBOutlet NSSlider *modWord20Slider;
@property (assign) IBOutlet NSSlider *modWord21Slider;
@property (assign) IBOutlet NSSlider *modWord22Slider;
@property (assign) IBOutlet NSSlider *modWord23Slider;
@property (assign) IBOutlet NSSlider *modWord24Slider;
@property (assign) IBOutlet NSSlider *modWord25Slider;
@property (assign) IBOutlet NSSlider *modWord26Slider;
@property (assign) IBOutlet NSSlider *modWord27Slider;
@property (assign) IBOutlet NSSlider *modWord28Slider;
@property (assign) IBOutlet NSSlider *modWord29Slider;
@property (assign) IBOutlet NSSlider *modWord30Slider;
@property (assign) IBOutlet NSSlider *modWord31Slider;
@property (assign) IBOutlet NSSlider *modWord32Slider;

@property (assign) IBOutlet NSStepper *modWord1Stepper;
@property (assign) IBOutlet NSStepper *modWord2Stepper;
@property (assign) IBOutlet NSStepper *modWord3Stepper;
@property (assign) IBOutlet NSStepper *modWord4Stepper;
@property (assign) IBOutlet NSStepper *modWord5Stepper;
@property (assign) IBOutlet NSStepper *modWord6Stepper;
@property (assign) IBOutlet NSStepper *modWord7Stepper;
@property (assign) IBOutlet NSStepper *modWord8Stepper;
@property (assign) IBOutlet NSStepper *modWord9Stepper;
@property (assign) IBOutlet NSStepper *modWord10Stepper;
@property (assign) IBOutlet NSStepper *modWord11Stepper;
@property (assign) IBOutlet NSStepper *modWord12Stepper;
@property (assign) IBOutlet NSStepper *modWord13Stepper;
@property (assign) IBOutlet NSStepper *modWord14Stepper;
@property (assign) IBOutlet NSStepper *modWord15Stepper;
@property (assign) IBOutlet NSStepper *modWord16Stepper;
@property (assign) IBOutlet NSStepper *modWord17Stepper;
@property (assign) IBOutlet NSStepper *modWord18Stepper;
@property (assign) IBOutlet NSStepper *modWord19Stepper;
@property (assign) IBOutlet NSStepper *modWord20Stepper;
@property (assign) IBOutlet NSStepper *modWord21Stepper;
@property (assign) IBOutlet NSStepper *modWord22Stepper;
@property (assign) IBOutlet NSStepper *modWord23Stepper;
@property (assign) IBOutlet NSStepper *modWord24Stepper;
@property (assign) IBOutlet NSStepper *modWord25Stepper;
@property (assign) IBOutlet NSStepper *modWord26Stepper;
@property (assign) IBOutlet NSStepper *modWord27Stepper;
@property (assign) IBOutlet NSStepper *modWord28Stepper;
@property (assign) IBOutlet NSStepper *modWord29Stepper;
@property (assign) IBOutlet NSStepper *modWord30Stepper;
@property (assign) IBOutlet NSStepper *modWord31Stepper;
@property (assign) IBOutlet NSStepper *modWord32Stepper;

@property (assign) IBOutlet NSButton *ch1EnabledCheckBox;
@property (assign) IBOutlet NSTextField *ch1XTextField;
@property (assign) IBOutlet NSTextField *ch1HzTextField;
@property (assign) IBOutlet NSPopUpButton *ch1WaveRAMPopUp;
@property (assign) IBOutlet NSButton *ch1IntervalCheckBox;
@property (assign) IBOutlet NSPopUpButton *ch1IntervalPopUp;
@property (assign) IBOutlet NSSlider *ch1LeftVolSlider;
@property (assign) IBOutlet NSSlider *ch1RightVolSlider;

@property (assign) IBOutlet NSButton *ch1EnvEnabledCheckBox;
@property (assign) IBOutlet NSButton *ch1EnvRepeatingCheckBox;
@property (assign) IBOutlet NSMatrix *ch1EnvDirectionRadios;
@property (assign) IBOutlet NSPopUpButton *ch1EnvStepTimePopUp;
@property (assign) IBOutlet NSSlider *ch1EnvInitialValSlider;

@property (assign) IBOutlet NSTextField *s1intTextField;
@property (assign) IBOutlet NSTextField *s1lrvTextField;
@property (assign) IBOutlet NSTextField *s1fqhTextField;
@property (assign) IBOutlet NSTextField *s1fqlTextField;
@property (assign) IBOutlet NSTextField *s1ev0TextField;
@property (assign) IBOutlet NSTextField *s1ev1TextField;
@property (assign) IBOutlet NSTextField *s1ramTextField;

@property (assign) IBOutlet NSButton *ch2EnabledCheckBox;
@property (assign) IBOutlet NSTextField *ch2XTextField;
@property (assign) IBOutlet NSTextField *ch2HzTextField;
@property (assign) IBOutlet NSPopUpButton *ch2WaveRAMPopUp;
@property (assign) IBOutlet NSButton *ch2IntervalCheckBox;
@property (assign) IBOutlet NSPopUpButton *ch2IntervalPopUp;
@property (assign) IBOutlet NSSlider *ch2LeftVolSlider;
@property (assign) IBOutlet NSSlider *ch2RightVolSlider;

@property (assign) IBOutlet NSButton *ch2EnvEnabledCheckBox;
@property (assign) IBOutlet NSButton *ch2EnvRepeatingCheckBox;
@property (assign) IBOutlet NSMatrix *ch2EnvDirectionRadios;
@property (assign) IBOutlet NSPopUpButton *ch2EnvStepTimePopUp;
@property (assign) IBOutlet NSSlider *ch2EnvInitialValSlider;

@property (assign) IBOutlet NSTextField *s2intTextField;
@property (assign) IBOutlet NSTextField *s2lrvTextField;
@property (assign) IBOutlet NSTextField *s2fqhTextField;
@property (assign) IBOutlet NSTextField *s2fqlTextField;
@property (assign) IBOutlet NSTextField *s2ev0TextField;
@property (assign) IBOutlet NSTextField *s2ev1TextField;
@property (assign) IBOutlet NSTextField *s2ramTextField;

@property (assign) IBOutlet NSButton *ch3EnabledCheckBox;
@property (assign) IBOutlet NSTextField *ch3XTextField;
@property (assign) IBOutlet NSTextField *ch3HzTextField;
@property (assign) IBOutlet NSPopUpButton *ch3WaveRAMPopUp;
@property (assign) IBOutlet NSButton *ch3IntervalCheckBox;
@property (assign) IBOutlet NSPopUpButton *ch3IntervalPopUp;
@property (assign) IBOutlet NSSlider *ch3LeftVolSlider;
@property (assign) IBOutlet NSSlider *ch3RightVolSlider;

@property (assign) IBOutlet NSButton *ch3EnvEnabledCheckBox;
@property (assign) IBOutlet NSButton *ch3EnvRepeatingCheckBox;
@property (assign) IBOutlet NSMatrix *ch3EnvDirectionRadios;
@property (assign) IBOutlet NSPopUpButton *ch3EnvStepTimePopUp;
@property (assign) IBOutlet NSSlider *ch3EnvInitialValSlider;

@property (assign) IBOutlet NSTextField *s3intTextField;
@property (assign) IBOutlet NSTextField *s3lrvTextField;
@property (assign) IBOutlet NSTextField *s3fqhTextField;
@property (assign) IBOutlet NSTextField *s3fqlTextField;
@property (assign) IBOutlet NSTextField *s3ev0TextField;
@property (assign) IBOutlet NSTextField *s3ev1TextField;
@property (assign) IBOutlet NSTextField *s3ramTextField;

@property (assign) IBOutlet NSButton *ch4EnabledCheckBox;
@property (assign) IBOutlet NSTextField *ch4XTextField;
@property (assign) IBOutlet NSTextField *ch4HzTextField;
@property (assign) IBOutlet NSPopUpButton *ch4WaveRAMPopUp;
@property (assign) IBOutlet NSButton *ch4IntervalCheckBox;
@property (assign) IBOutlet NSPopUpButton *ch4IntervalPopUp;
@property (assign) IBOutlet NSSlider *ch4LeftVolSlider;
@property (assign) IBOutlet NSSlider *ch4RightVolSlider;

@property (assign) IBOutlet NSButton *ch4EnvEnabledCheckBox;
@property (assign) IBOutlet NSButton *ch4EnvRepeatingCheckBox;
@property (assign) IBOutlet NSMatrix *ch4EnvDirectionRadios;
@property (assign) IBOutlet NSPopUpButton *ch4EnvStepTimePopUp;
@property (assign) IBOutlet NSSlider *ch4EnvInitialValSlider;

@property (assign) IBOutlet NSTextField *s4intTextField;
@property (assign) IBOutlet NSTextField *s4lrvTextField;
@property (assign) IBOutlet NSTextField *s4fqhTextField;
@property (assign) IBOutlet NSTextField *s4fqlTextField;
@property (assign) IBOutlet NSTextField *s4ev0TextField;
@property (assign) IBOutlet NSTextField *s4ev1TextField;
@property (assign) IBOutlet NSTextField *s4ramTextField;

@property (assign) IBOutlet NSButton *ch5EnabledCheckBox;
@property (assign) IBOutlet NSTextField *ch5XTextField;
@property (assign) IBOutlet NSTextField *ch5HzTextField;
@property (assign) IBOutlet NSPopUpButton *ch5WaveRAMPopUp;
@property (assign) IBOutlet NSButton *ch5IntervalCheckBox;
@property (assign) IBOutlet NSPopUpButton *ch5IntervalPopUp;
@property (assign) IBOutlet NSSlider *ch5LeftVolSlider;
@property (assign) IBOutlet NSSlider *ch5RightVolSlider;

@property (assign) IBOutlet NSButton *ch5EnvEnabledCheckBox;
@property (assign) IBOutlet NSButton *ch5EnvRepeatingCheckBox;
@property (assign) IBOutlet NSMatrix *ch5EnvDirectionRadios;
@property (assign) IBOutlet NSPopUpButton *ch5EnvStepTimePopUp;
@property (assign) IBOutlet NSSlider *ch5EnvInitialValSlider;

@property (assign) IBOutlet NSButton *ch5SweepEnabledCheckBox;
@property (assign) IBOutlet NSButton *ch5SweepRepeatingCheckBox;
@property (assign) IBOutlet NSMatrix *ch5SweepFunctionRadios;
@property (assign) IBOutlet NSMatrix *ch5SweepDirectionRadios;
@property (assign) IBOutlet NSPopUpButton *ch5SweepFreqPopUp;
@property (assign) IBOutlet NSPopUpButton *ch5SweepIntervalPopUp;
@property (assign) IBOutlet NSSlider *ch5SweepShiftSlider;

@property (assign) IBOutlet NSTextField *s5intTextField;
@property (assign) IBOutlet NSTextField *s5lrvTextField;
@property (assign) IBOutlet NSTextField *s5fqhTextField;
@property (assign) IBOutlet NSTextField *s5fqlTextField;
@property (assign) IBOutlet NSTextField *s5ev0TextField;
@property (assign) IBOutlet NSTextField *s5ev1TextField;
@property (assign) IBOutlet NSTextField *s5ramTextField;
@property (assign) IBOutlet NSTextField *s5swpTextField;

@property (assign) IBOutlet NSButton *ch6EnabledCheckBox;
@property (assign) IBOutlet NSTextField *ch6XTextField;
@property (assign) IBOutlet NSTextField *ch6HzTextField;
@property (assign) IBOutlet NSPopUpButton *ch6TapLocationPopUp;
@property (assign) IBOutlet NSButton *ch6IntervalCheckBox;
@property (assign) IBOutlet NSPopUpButton *ch6IntervalPopUp;
@property (assign) IBOutlet NSSlider *ch6LeftVolSlider;
@property (assign) IBOutlet NSSlider *ch6RightVolSlider;

@property (assign) IBOutlet NSButton *ch6EnvEnabledCheckBox;
@property (assign) IBOutlet NSButton *ch6EnvRepeatingCheckBox;
@property (assign) IBOutlet NSMatrix *ch6EnvDirectionRadios;
@property (assign) IBOutlet NSPopUpButton *ch6EnvStepTimePopUp;
@property (assign) IBOutlet NSSlider *ch6EnvInitialValSlider;

@property (assign) IBOutlet NSTextField *s6intTextField;
@property (assign) IBOutlet NSTextField *s6lrvTextField;
@property (assign) IBOutlet NSTextField *s6fqhTextField;
@property (assign) IBOutlet NSTextField *s6fqlTextField;
@property (assign) IBOutlet NSTextField *s6ev0TextField;
@property (assign) IBOutlet NSTextField *s6ev1TextField;

- (IBAction)makeSineWave:(id)sender;
- (IBAction)makeSquareWave:(id)sender;
- (IBAction)makeTriangleWave:(id)sender;
- (IBAction)makeSawtoothWave:(id)sender;

- (IBAction)takeIntValueForWaveWord1From:(id)sender;
- (IBAction)takeIntValueForWaveWord2From:(id)sender;
- (IBAction)takeIntValueForWaveWord3From:(id)sender;
- (IBAction)takeIntValueForWaveWord4From:(id)sender;
- (IBAction)takeIntValueForWaveWord5From:(id)sender;
- (IBAction)takeIntValueForWaveWord6From:(id)sender;
- (IBAction)takeIntValueForWaveWord7From:(id)sender;
- (IBAction)takeIntValueForWaveWord8From:(id)sender;
- (IBAction)takeIntValueForWaveWord9From:(id)sender;
- (IBAction)takeIntValueForWaveWord10From:(id)sender;
- (IBAction)takeIntValueForWaveWord11From:(id)sender;
- (IBAction)takeIntValueForWaveWord12From:(id)sender;
- (IBAction)takeIntValueForWaveWord13From:(id)sender;
- (IBAction)takeIntValueForWaveWord14From:(id)sender;
- (IBAction)takeIntValueForWaveWord15From:(id)sender;
- (IBAction)takeIntValueForWaveWord16From:(id)sender;
- (IBAction)takeIntValueForWaveWord17From:(id)sender;
- (IBAction)takeIntValueForWaveWord18From:(id)sender;
- (IBAction)takeIntValueForWaveWord19From:(id)sender;
- (IBAction)takeIntValueForWaveWord20From:(id)sender;
- (IBAction)takeIntValueForWaveWord21From:(id)sender;
- (IBAction)takeIntValueForWaveWord22From:(id)sender;
- (IBAction)takeIntValueForWaveWord23From:(id)sender;
- (IBAction)takeIntValueForWaveWord24From:(id)sender;
- (IBAction)takeIntValueForWaveWord25From:(id)sender;
- (IBAction)takeIntValueForWaveWord26From:(id)sender;
- (IBAction)takeIntValueForWaveWord27From:(id)sender;
- (IBAction)takeIntValueForWaveWord28From:(id)sender;
- (IBAction)takeIntValueForWaveWord29From:(id)sender;
- (IBAction)takeIntValueForWaveWord30From:(id)sender;
- (IBAction)takeIntValueForWaveWord31From:(id)sender;
- (IBAction)takeIntValueForWaveWord32From:(id)sender;

- (IBAction)takeStateForEnabledFrom:(id)sender;
- (IBAction)takeIntValueForXFrom:(id)sender;
- (IBAction)takeFloatValueForHzFrom:(id)sender;
- (IBAction)takeIntValueForWaveRAMFrom:(id)sender;
- (IBAction)takeIntValueForTapLocationFrom:(id)sender;
- (IBAction)takeStateForIntervalFrom:(id)sender;
- (IBAction)takeIntValueForIntervalDataFrom:(id)sender;
- (IBAction)takeIntValueForLeftVolFrom:(id)sender;
- (IBAction)takeIntValueForRightVolFrom:(id)sender;

- (IBAction)takeStateForEnvelopeFrom:(id)sender;
- (IBAction)takeStateForEnvRepeatFrom:(id)sender;
- (IBAction)takeIntValueForEnvDirectionFrom:(id)sender;
- (IBAction)takeIntValueForEnvStepTimeFrom:(id)sender;
- (IBAction)takeIntValueForEnvInitialValFrom:(id)sender;

- (IBAction)takeStateForSweepFrom:(id)sender;
- (IBAction)takeStateForSweepRepeatFrom:(id)sender;
- (IBAction)takeIntValueForSweepFunctionFrom:(id)sender;
- (IBAction)takeIntValueForSweepDirectionFrom:(id)sender;
- (IBAction)takeIntValueForSweepFreqFrom:(id)sender;
- (IBAction)takeIntValueForSweepIntervalFrom:(id)sender;
- (IBAction)takeIntValueForSweepShiftFrom:(id)sender;

- (IBAction)takeIntValueForIntRegFrom:(id)sender;
- (IBAction)takeIntValueForLRVRegFrom:(id)sender;
- (IBAction)takeIntValueForFQHRegFrom:(id)sender;
- (IBAction)takeIntValueForFQLRegFrom:(id)sender;
- (IBAction)takeIntValueForEV0RegFrom:(id)sender;
- (IBAction)takeIntValueForEV1RegFrom:(id)sender;
- (IBAction)takeIntValueForRAMRegFrom:(id)sender;
- (IBAction)takeIntValueForSWPRegFrom:(id)sender;

@end
