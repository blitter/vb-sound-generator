//
//  BLTVSUAudioEngine.cpp
//  VB Sound Generator
//
//  Created by Keith Kaisershot on 3/8/14.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "BLTVSUAudioEngine.h"

#include <portaudio.h>

#include "BLTVSUTables.h"
#include "BLTVSUUtility.h"

#define CHECK_FOR_PA_ERROR \
    if (error != paNoError) \
    { \
        NSLog(@"PortAudio error on line %i: %s", __LINE__, Pa_GetErrorText(error)); \
    }

// internal state
typedef struct
{
	u32 FRQ_CNT;
    u32 RAM_ADDR;
    u32 INT_CNT;
    u32 ENV_CNT;
    u32 MOD_CNT;
    u32 MOD_ADDR;
    u32 l_vol;
    u32 r_vol;
    s32 l_out;
    s32 r_out;
} internal_state;

vsu_regs int_vsu;

internal_state int_state[6];

int sample_clk_cnt = 0;

PaStream* stream;

static const int VSU_SAMPLE_RATE = 41700;

bool isPlaying = false;

void process(u8* buffer, int length, int wavcnt);
int serviceAudioBuffers(const void* inputBuffer, void* outputBuffer, unsigned long frameCount, const PaStreamCallbackTimeInfo* timeInfo, PaStreamCallbackFlags statusFlags, void* userData);

@implementation BLTVSUAudioEngine

- (id)init
{
    if (self = [super init])
    {
        PaError error = paNoError;
        error = Pa_Initialize();
        CHECK_FOR_PA_ERROR;
        
        error = Pa_OpenDefaultStream(&stream, 0, 2, paInt16, VSU_SAMPLE_RATE, paFramesPerBufferUnspecified, serviceAudioBuffers, NULL);
        CHECK_FOR_PA_ERROR;
    }
    return self;
}

- (void)dealloc
{
    PaError error = paNoError;
    error = Pa_CloseStream(stream);
    CHECK_FOR_PA_ERROR;
    error = Pa_Terminate();
    CHECK_FOR_PA_ERROR;
}

- (BOOL)start:(vsu_regs &)vsu
{
    if (!isPlaying)
    {
        PaError error = paNoError;
        
        int_vsu = vsu;
        memset(&int_state, 0, sizeof(int_state));
        error = Pa_StartStream(stream);
        CHECK_FOR_PA_ERROR;
        isPlaying = true;
        return isPlaying;
    }
    else
    {
        return false;
    }
}

- (void)update:(vsu_regs &)vsu
{
    int_vsu = vsu;
}

- (void)stop
{
    if (isPlaying)
    {
        PaError error = paNoError;
        
        error = Pa_StopStream(stream);
        CHECK_FOR_PA_ERROR;
        isPlaying = false;
    }
}

@end

// creates output, length in samples, wavcnt is number of 5MHz counts per wav sample (120 for native 41.7KHz)
void process(u8* buffer, int length, int wavcnt)
{
    int loopcnt, temp, bufferPos = 0;
    
    loopcnt = length * wavcnt;
        
    for (int i = 0; i < loopcnt; i++)
    {
        for (int k = 0; k < 6; k++) // loop through all channels
        {
            if (isEnabled(int_vsu.sxint[k]))    // channel enabled
            {
                // frequency and RAM address section
                if (k < 4)  // normal channels
                {
                    if (int_state[k].FRQ_CNT >= (2048 - xFromFQHLReg(int_vsu.sxfqh[k], int_vsu.sxfql[k])))  // go to next address
                    {
                        int_state[k].RAM_ADDR = (int_state[k].RAM_ADDR + 1) & 0x1F;
                        int_state[k].FRQ_CNT = 0;
                    }
                }
                else if (k == 4)    // sweep/modulation channel
                {
                    if (modulationFunction(int_vsu.sxev1[k]))   // modulation enabled
                    {
                        temp= xFromFQHLReg(int_vsu.sxfqh[k], int_vsu.sxfql[k]) + vsu.waveram[5][int_state[k].MOD_ADDR];
                        if (temp > 0x7FF)
                        {
                            temp = 0x7FF;
                        }
                        else if (temp < 0)
                        {
                            temp = 0;
                        }
                        if (int_state[k].FRQ_CNT >= (2048 - temp))  // go to next address
                        {
                            int_state[k].RAM_ADDR = (int_state[k].RAM_ADDR + 1) & 0x1F;
                            int_state[k].FRQ_CNT = 0;
                        }
                    }
                    else    // normal frequency
                    {
                        if (int_state[k].FRQ_CNT >= (2048 - xFromFQHLReg(int_vsu.sxfqh[k], int_vsu.sxfql[k]))) // go to next address
                        {
                            int_state[k].RAM_ADDR = (int_state[k].RAM_ADDR + 1) & 0x1F;
                            int_state[k].FRQ_CNT = 0;
                        }
                    }
                    
                    if (isSweepEnabled(int_vsu.sxev1[k]) && (sweepInterval(int_vsu.s5swp) != 0))    // sweep/modulation enabled (get next sweep/modulation)
                    {
                        int_state[k].MOD_CNT++;
                        if ((sweepFreq(int_vsu.s5swp) && (int_state[k].MOD_CNT >= (38400 * sweepInterval(int_vsu.s5swp)))) || (!sweepFreq(int_vsu.s5swp) && (int_state[k].MOD_CNT >= (4800 * sweepInterval(int_vsu.s5swp)))))   // next sweep/modulation step
                        {
                            if (modulationFunction(int_vsu.sxev1[k]))   // modulation enabled
                            {
                                int_state[k].MOD_ADDR++;
                                if (int_state[k].MOD_ADDR >= 32)
                                {
                                    if (isSweepRepeating(int_vsu.sxev1[k])) // loop modulation
                                    {
                                        int_state[k].MOD_ADDR = 0;
                                    }
                                    else
                                    {
                                        int_state[k].MOD_ADDR = 31;
                                    }
                                }
                            }
                            else    // sweep enabled
                            {
                                temp = xFromFQHLReg(int_vsu.sxfqh[k], int_vsu.sxfql[k]);
                                if (sweepDirection(int_vsu.s5swp) == 1) // add
                                {
                                    temp += (temp >> sweepShift(int_vsu.s5swp));
                                }
                                else    // subtract
                                {
                                    temp -= (temp >> sweepShift(int_vsu.s5swp));
                                }
                                if (temp > 0x7FF)
                                {
                                    int_vsu.sxint[k] &= ~0x80;  // end of sweep/modulation, disable sound (next sample)
                                }
                                else
                                {
                                    int_vsu.sxfqh[k] = computeFQHReg(temp);
                                    int_vsu.sxfql[k] = computeFQLReg(temp);
                                }
                            }
                            int_state[k].MOD_CNT = 0;
                        }
                    }
                }
                else    // noise channel
                {
                    if (int_state[k].FRQ_CNT >= 10 * (2048 - xFromFQHLReg(int_vsu.sxfqh[k], int_vsu.sxfql[k]))) //go to next address (LFSR bit)
                    {
                        int_state[k].RAM_ADDR = (int_state[k].RAM_ADDR + 1) % noise_len[tapLocation(int_vsu.sxev1[k])];
                        int_state[k].FRQ_CNT = 0;
                    }
                }
                int_state[k].FRQ_CNT++;
                
                // envelope section
                if (isEnvelopeEnabled(int_vsu.sxev1[k]))    // envelope enabled
                {
                    if (int_state[k].ENV_CNT >= (76800 * (envStepTime(int_vsu.sxev0[k]) + 1)))  // next envelope step
                    {
                        temp = envInitialValue(int_vsu.sxev0[k]);
                        if (envDirection(int_vsu.sxev0[k])) // grow
                        {
                            temp++;
                            if (temp > 15)  // grew to max
                            {
                                if (isEnvelopeRepeating(int_vsu.sxev1[k]))  // repeat
                                {
                                    temp = 0;
                                }
                                else    // no repeat, stay at max and stop envelope
                                {
                                    temp = 15;
                                    int_vsu.sxev1[k] &= ~0x01;
                                }
                            }
                        }
                        else    // decay
                        {
                            temp--;
                            if (temp < 0)   // decayed to min
                            {
                                if (isEnvelopeRepeating(int_vsu.sxev1[k]))  // repeat
                                {
                                    temp = 15;
                                }
                                else    // no repeat, stay at min and stop envelope
                                {
                                    temp = 0;
                                    int_vsu.sxev1[k] &= ~0x01;
                                }
                            }
                        }
                        int_vsu.sxev0[k] = computeEV0Reg(temp, envDirection(int_vsu.sxev0[k]), envStepTime(int_vsu.sxev0[k]));
                        int_state[k].ENV_CNT = 0;   // reset count
                    }
                    int_state[k].ENV_CNT++;
                }
                
                // volume section
                if ((leftVolume(int_vsu.sxlrv[k]) == 0) || (envInitialValue(int_vsu.sxev0[k]) == 0))
                {
                    int_state[k].l_vol = 0;
                }
                else
                {
                    int_state[k].l_vol = ((leftVolume(int_vsu.sxlrv[k]) * envInitialValue(int_vsu.sxev0[k])) >> 3) + 1;
                }
                if ((rightVolume(int_vsu.sxlrv[k]) == 0) || (envInitialValue(int_vsu.sxev0[k]) == 0))
                {
                    int_state[k].r_vol = 0;
                }
                else
                {
                    int_state[k].r_vol = ((rightVolume(int_vsu.sxlrv[k]) * envInitialValue(int_vsu.sxev0[k])) >> 3) + 1;
                }
                
                // sound interval section
                if (isIntervalEnabled(int_vsu.sxint[k]))    // use sound interval data
                {
                    int_state[k].INT_CNT++;
                    if (int_state[k].INT_CNT >= (19200 * (intervalData(int_vsu.sxint[k]) + 1)))
                    {
                        int_vsu.sxint[k] &= ~0x80;  // end of interval, disable sound (next sample)
                    }
                }
                if (k != 5)
                {
                    int_state[k].l_out = (int_state[k].l_vol * int_vsu.waveram[waveRAM(int_vsu.sxram[k])][int_state[k].RAM_ADDR]) >> 1;
                    int_state[k].r_out = (int_state[k].r_vol * int_vsu.waveram[waveRAM(int_vsu.sxram[k])][int_state[k].RAM_ADDR]) >> 1;
                }
                else
                {
                    int_state[k].l_out = (int_state[k].l_vol * noise[tapLocation(int_vsu.sxev1[k])][int_state[k].RAM_ADDR]) >> 1;
                    int_state[k].r_out = (int_state[k].r_vol * noise[tapLocation(int_vsu.sxev1[k])][int_state[k].RAM_ADDR]) >> 1;
                }
            }
            else
            {
                int_state[k].l_out = 0;
                int_state[k].r_out = 0;
            }
        }
        sample_clk_cnt++;
        if (sample_clk_cnt >= wavcnt)
        {
            temp = ((int_state[0].l_out + int_state[1].l_out + int_state[2].l_out + int_state[3].l_out + int_state[4].l_out + int_state[5].l_out) << 2);    // make 13 bits into full scale 16 bit wav (no negative)
            buffer[bufferPos++] = temp & 0xFF;
            buffer[bufferPos++] = temp >> 8;
            temp = ((int_state[0].r_out + int_state[1].r_out + int_state[2].r_out + int_state[3].r_out + int_state[4].r_out + int_state[5].r_out) << 2);    //make 13 bits into full scale 16 bit wav (no negative)
            buffer[bufferPos++] = temp & 0xFF;
            buffer[bufferPos++] = temp >> 8;
            sample_clk_cnt = 0;
        }
    }
}

int serviceAudioBuffers(const void* inputBuffer, void* outputBuffer, unsigned long frameCount, const PaStreamCallbackTimeInfo* timeInfo, PaStreamCallbackFlags statusFlags, void* userData)
{
    process((u8*)outputBuffer, (int)frameCount, 120);
    return 0;
}
