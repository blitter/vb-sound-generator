//
//  BLTVSUUtility.cpp
//  VB Sound Generator
//
//  Created by Keith Kaisershot on 2/23/14.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#include <iostream>
#include <cmath>

#include "BLTVSUUtility.h"

vsu_regs vsu;

int computeWaveWord(int data)
{
    return data & 0xFF;
}

int waveData(int word)
{
    return word & 0xFF;
}

int computeXFromFreq(float hz)
{
    return (int)roundf(2048.0f - 5000000.0f / (hz * 32.0f));
}

float computeFreqFromX(int x)
{
    return 5000000.0f / ((2048.0f - x) * 32.0f);
}

int computeXFromNoiseFreq(float hz)
{
    return (int)roundf(2048.0f - 500000.0f / hz);
}

float computeNoiseFreqFromX(int x)
{
    return 500000.0f / (2048.0f - x);
}

int computeIntReg(bool enabled, bool interval, int intervalData)
{
    int intReg = 0x00;
    if (enabled)
    {
        intReg |= 0x80;
    }
    if (interval)
    {
        intReg |= 0x20;
    }
    intReg |= intervalData & 0x1F;
    return intReg;
}

bool isEnabled(int intReg)
{
    return (intReg & 0x80) != 0;
}

bool isIntervalEnabled(int intReg)
{
    return (intReg & 0x20) != 0;
}

int intervalData(int intReg)
{
    return intReg & 0x1F;
}

int computeLRVReg(int leftVolume, int rightVolume)
{
    return ((leftVolume & 0x0F) << 4) | (rightVolume & 0x0F);
}

int leftVolume(int lrvReg)
{
    return (lrvReg & 0xF0) >> 4;
}

int rightVolume(int lrvReg)
{
    return lrvReg & 0x0F;
}

int computeFQHReg(int x)
{
    return (x & 0x0700) >> 8;
}

int computeFQLReg(int x)
{
    return x & 0x00FF;
}

int xFromFQHLReg(int fqhReg, int fqlReg)
{
    return ((fqhReg & 0x07) << 8) | (fqlReg & 0xFF);
}

int computeEV0Reg(int envInitialValue, int envDirection, int envStepTime)
{
    return ((envInitialValue & 0x0F) << 4) | ((envDirection & 0x01) << 3) | (envStepTime & 0x07);
}

int envInitialValue(int ev0Reg)
{
    return (ev0Reg & 0xF0) >> 4;
}

int envDirection(int ev0Reg)
{
    return (ev0Reg & 0x08) >> 3;
}

int envStepTime(int ev0Reg)
{
    return ev0Reg & 0x07;
}

int computeEV1Reg(bool envelope, bool repeat, bool sweepEnabled, bool sweepRepeat, int sweepFunction)
{
    int ev1Reg = 0x00;
    if (envelope)
    {
        ev1Reg |= 0x01;
    }
    if (repeat)
    {
        ev1Reg |= 0x02;
    }
    if (sweepEnabled)
    {
        ev1Reg |= 0x40;
    }
    if (sweepRepeat)
    {
        ev1Reg |= 0x20;
    }
    ev1Reg |= (sweepFunction & 0x01) << 4;
    return ev1Reg;
}

int computeEV1Reg(bool envelope, bool repeat, int tapLocation)
{
    int ev1Reg = 0x00;
    if (envelope)
    {
        ev1Reg |= 0x01;
    }
    if (repeat)
    {
        ev1Reg |= 0x02;
    }
    ev1Reg |= (tapLocation & 0x07) << 4;
    return ev1Reg;
}

bool isEnvelopeEnabled(int ev1Reg)
{
    return (ev1Reg & 0x01) != 0;
}

bool isEnvelopeRepeating(int ev1Reg)
{
    return (ev1Reg & 0x02) != 0;
}

bool isSweepEnabled(int ev1Reg)
{
    return (ev1Reg & 0x40) != 0;
}

bool isSweepRepeating(int ev1Reg)
{
    return (ev1Reg & 0x20) != 0;
}

int modulationFunction(int ev1Reg)
{
    return (ev1Reg & 0x10) >> 4;
}

int tapLocation(int ev1Reg)
{
    return (ev1Reg & 0x70) >> 4;
}

int computeWaveRAM(int waveRAM)
{
    return waveRAM & 0x0F;
}

int waveRAM(int waveRAM)
{
    return waveRAM & 0x0F;
}

int computeSWPReg(int sweepFreq, int sweepInterval, int sweepDirection, int sweepShift)
{
    return ((sweepFreq & 0x01) << 7) | ((sweepInterval & 0x07) << 4) | ((sweepDirection & 0x01) << 3) | (sweepShift & 0x07);
}

int sweepFreq(int swpReg)
{
    return (swpReg & 0x80) >> 7;
}

int sweepInterval(int swpReg)
{
    return (swpReg & 0x70) >> 4;
}

int sweepDirection(int swpReg)
{
    return (swpReg & 0x08) >> 3;
}

int sweepShift(int swpReg)
{
    return swpReg & 0x07;
}
