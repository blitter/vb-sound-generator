//
//  BLTVSUUtility.h
//  VB Sound Generator
//
//  Created by Keith Kaisershot on 2/23/14.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#ifndef VB_Sound_Generator_BLTVSUUtility_h
#define VB_Sound_Generator_BLTVSUUtility_h

static const int NUM_VSU_CHANNELS = 6;

typedef struct vsu_regs
{
    int waveram[6][32];

    int sxint[NUM_VSU_CHANNELS];
    int sxlrv[NUM_VSU_CHANNELS];
    int sxfqh[NUM_VSU_CHANNELS];
    int sxfql[NUM_VSU_CHANNELS];
    int sxev0[NUM_VSU_CHANNELS];
    int sxev1[NUM_VSU_CHANNELS];
    int sxram[NUM_VSU_CHANNELS - 1];
    int s5swp;
} vsu_regs;

extern vsu_regs vsu;

int computeWaveWord(int data);
int waveData(int word);

int computeXFromFreq(float hz);
float computeFreqFromX(int x);
int computeXFromNoiseFreq(float hz);
float computeNoiseFreqFromX(int x);

int computeIntReg(bool enabled, bool interval, int intervalData);
bool isEnabled(int intReg);
bool isIntervalEnabled(int intReg);
int intervalData(int intReg);

int computeLRVReg(int leftVolume, int rightVolume);
int leftVolume(int lrvReg);
int rightVolume(int lrvReg);

int computeFQHReg(int x);
int computeFQLReg(int x);
int xFromFQHLReg(int fqhReg, int fqlReg);

int computeEV0Reg(int envInitialValue, int envDirection, int envStepTime);
int envInitialValue(int ev0Reg);
int envDirection(int ev0Reg);
int envStepTime(int ev0Reg);

int computeEV1Reg(bool envelope, bool repeat, bool sweepEnabled = false, bool sweepRepeat = false, int sweepFunction = 0);
int computeEV1Reg(bool envelope, bool repeat, int tapLocation);
bool isEnvelopeEnabled(int ev1Reg);
bool isEnvelopeRepeating(int ev1Reg);
bool isSweepEnabled(int ev1Reg);
bool isSweepRepeating(int ev1Reg);
int modulationFunction(int ev1Reg);
int tapLocation(int ev1Reg);

int computeWaveRAM(int waveRAM);
int waveRAM(int waveRAM);

int computeSWPReg(int sweepFreq, int sweepInterval, int sweepDirection, int sweepShift);
int sweepFreq(int swpReg);
int sweepInterval(int swpReg);
int sweepDirection(int swpReg);
int sweepShift(int swpReg);

#endif
