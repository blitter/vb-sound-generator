//
//  BLTAppDelegate.m
//  VB Sound Generator
//
//  Created by Keith Kaisershot on 2/20/14.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "BLTAppDelegate.h"

#import "BLTVSUAudioEngine.h"

#include "BLTVSUUtility.h"

@interface BLTAppDelegate ()

@property (nonatomic, strong) BLTVSUAudioEngine* audioEngine;

@end

@implementation BLTAppDelegate

@synthesize window = _window;

@synthesize wave1SineButton = _wave1SineButton;
@synthesize wave1SquareButton = _wave1SquareButton;
@synthesize wave1TriangleButton = _wave1TriangleButton;
@synthesize wave1SawtoothButton = _wave1SawtoothButton;

@synthesize wave1Word1Slider = _wave1Word1Slider;
@synthesize wave1Word2Slider = _wave1Word2Slider;
@synthesize wave1Word3Slider = _wave1Word3Slider;
@synthesize wave1Word4Slider = _wave1Word4Slider;
@synthesize wave1Word5Slider = _wave1Word5Slider;
@synthesize wave1Word6Slider = _wave1Word6Slider;
@synthesize wave1Word7Slider = _wave1Word7Slider;
@synthesize wave1Word8Slider = _wave1Word8Slider;
@synthesize wave1Word9Slider = _wave1Word9Slider;
@synthesize wave1Word10Slider = _wave1Word10Slider;
@synthesize wave1Word11Slider = _wave1Word11Slider;
@synthesize wave1Word12Slider = _wave1Word12Slider;
@synthesize wave1Word13Slider = _wave1Word13Slider;
@synthesize wave1Word14Slider = _wave1Word14Slider;
@synthesize wave1Word15Slider = _wave1Word15Slider;
@synthesize wave1Word16Slider = _wave1Word16Slider;
@synthesize wave1Word17Slider = _wave1Word17Slider;
@synthesize wave1Word18Slider = _wave1Word18Slider;
@synthesize wave1Word19Slider = _wave1Word19Slider;
@synthesize wave1Word20Slider = _wave1Word20Slider;
@synthesize wave1Word21Slider = _wave1Word21Slider;
@synthesize wave1Word22Slider = _wave1Word22Slider;
@synthesize wave1Word23Slider = _wave1Word23Slider;
@synthesize wave1Word24Slider = _wave1Word24Slider;
@synthesize wave1Word25Slider = _wave1Word25Slider;
@synthesize wave1Word26Slider = _wave1Word26Slider;
@synthesize wave1Word27Slider = _wave1Word27Slider;
@synthesize wave1Word28Slider = _wave1Word28Slider;
@synthesize wave1Word29Slider = _wave1Word29Slider;
@synthesize wave1Word30Slider = _wave1Word30Slider;
@synthesize wave1Word31Slider = _wave1Word31Slider;
@synthesize wave1Word32Slider = _wave1Word32Slider;

@synthesize wave1Word1Stepper = _wave1Word1Stepper;
@synthesize wave1Word2Stepper = _wave1Word2Stepper;
@synthesize wave1Word3Stepper = _wave1Word3Stepper;
@synthesize wave1Word4Stepper = _wave1Word4Stepper;
@synthesize wave1Word5Stepper = _wave1Word5Stepper;
@synthesize wave1Word6Stepper = _wave1Word6Stepper;
@synthesize wave1Word7Stepper = _wave1Word7Stepper;
@synthesize wave1Word8Stepper = _wave1Word8Stepper;
@synthesize wave1Word9Stepper = _wave1Word9Stepper;
@synthesize wave1Word10Stepper = _wave1Word10Stepper;
@synthesize wave1Word11Stepper = _wave1Word11Stepper;
@synthesize wave1Word12Stepper = _wave1Word12Stepper;
@synthesize wave1Word13Stepper = _wave1Word13Stepper;
@synthesize wave1Word14Stepper = _wave1Word14Stepper;
@synthesize wave1Word15Stepper = _wave1Word15Stepper;
@synthesize wave1Word16Stepper = _wave1Word16Stepper;
@synthesize wave1Word17Stepper = _wave1Word17Stepper;
@synthesize wave1Word18Stepper = _wave1Word18Stepper;
@synthesize wave1Word19Stepper = _wave1Word19Stepper;
@synthesize wave1Word20Stepper = _wave1Word20Stepper;
@synthesize wave1Word21Stepper = _wave1Word21Stepper;
@synthesize wave1Word22Stepper = _wave1Word22Stepper;
@synthesize wave1Word23Stepper = _wave1Word23Stepper;
@synthesize wave1Word24Stepper = _wave1Word24Stepper;
@synthesize wave1Word25Stepper = _wave1Word25Stepper;
@synthesize wave1Word26Stepper = _wave1Word26Stepper;
@synthesize wave1Word27Stepper = _wave1Word27Stepper;
@synthesize wave1Word28Stepper = _wave1Word28Stepper;
@synthesize wave1Word29Stepper = _wave1Word29Stepper;
@synthesize wave1Word30Stepper = _wave1Word30Stepper;
@synthesize wave1Word31Stepper = _wave1Word31Stepper;
@synthesize wave1Word32Stepper = _wave1Word32Stepper;

@synthesize wave2SineButton = _wave2SineButton;
@synthesize wave2SquareButton = _wave2SquareButton;
@synthesize wave2TriangleButton = _wave2TriangleButton;
@synthesize wave2SawtoothButton = _wave2SawtoothButton;

@synthesize wave2Word1Slider = _wave2Word1Slider;
@synthesize wave2Word2Slider = _wave2Word2Slider;
@synthesize wave2Word3Slider = _wave2Word3Slider;
@synthesize wave2Word4Slider = _wave2Word4Slider;
@synthesize wave2Word5Slider = _wave2Word5Slider;
@synthesize wave2Word6Slider = _wave2Word6Slider;
@synthesize wave2Word7Slider = _wave2Word7Slider;
@synthesize wave2Word8Slider = _wave2Word8Slider;
@synthesize wave2Word9Slider = _wave2Word9Slider;
@synthesize wave2Word10Slider = _wave2Word10Slider;
@synthesize wave2Word11Slider = _wave2Word11Slider;
@synthesize wave2Word12Slider = _wave2Word12Slider;
@synthesize wave2Word13Slider = _wave2Word13Slider;
@synthesize wave2Word14Slider = _wave2Word14Slider;
@synthesize wave2Word15Slider = _wave2Word15Slider;
@synthesize wave2Word16Slider = _wave2Word16Slider;
@synthesize wave2Word17Slider = _wave2Word17Slider;
@synthesize wave2Word18Slider = _wave2Word18Slider;
@synthesize wave2Word19Slider = _wave2Word19Slider;
@synthesize wave2Word20Slider = _wave2Word20Slider;
@synthesize wave2Word21Slider = _wave2Word21Slider;
@synthesize wave2Word22Slider = _wave2Word22Slider;
@synthesize wave2Word23Slider = _wave2Word23Slider;
@synthesize wave2Word24Slider = _wave2Word24Slider;
@synthesize wave2Word25Slider = _wave2Word25Slider;
@synthesize wave2Word26Slider = _wave2Word26Slider;
@synthesize wave2Word27Slider = _wave2Word27Slider;
@synthesize wave2Word28Slider = _wave2Word28Slider;
@synthesize wave2Word29Slider = _wave2Word29Slider;
@synthesize wave2Word30Slider = _wave2Word30Slider;
@synthesize wave2Word31Slider = _wave2Word31Slider;
@synthesize wave2Word32Slider = _wave2Word32Slider;

@synthesize wave2Word1Stepper = _wave2Word1Stepper;
@synthesize wave2Word2Stepper = _wave2Word2Stepper;
@synthesize wave2Word3Stepper = _wave2Word3Stepper;
@synthesize wave2Word4Stepper = _wave2Word4Stepper;
@synthesize wave2Word5Stepper = _wave2Word5Stepper;
@synthesize wave2Word6Stepper = _wave2Word6Stepper;
@synthesize wave2Word7Stepper = _wave2Word7Stepper;
@synthesize wave2Word8Stepper = _wave2Word8Stepper;
@synthesize wave2Word9Stepper = _wave2Word9Stepper;
@synthesize wave2Word10Stepper = _wave2Word10Stepper;
@synthesize wave2Word11Stepper = _wave2Word11Stepper;
@synthesize wave2Word12Stepper = _wave2Word12Stepper;
@synthesize wave2Word13Stepper = _wave2Word13Stepper;
@synthesize wave2Word14Stepper = _wave2Word14Stepper;
@synthesize wave2Word15Stepper = _wave2Word15Stepper;
@synthesize wave2Word16Stepper = _wave2Word16Stepper;
@synthesize wave2Word17Stepper = _wave2Word17Stepper;
@synthesize wave2Word18Stepper = _wave2Word18Stepper;
@synthesize wave2Word19Stepper = _wave2Word19Stepper;
@synthesize wave2Word20Stepper = _wave2Word20Stepper;
@synthesize wave2Word21Stepper = _wave2Word21Stepper;
@synthesize wave2Word22Stepper = _wave2Word22Stepper;
@synthesize wave2Word23Stepper = _wave2Word23Stepper;
@synthesize wave2Word24Stepper = _wave2Word24Stepper;
@synthesize wave2Word25Stepper = _wave2Word25Stepper;
@synthesize wave2Word26Stepper = _wave2Word26Stepper;
@synthesize wave2Word27Stepper = _wave2Word27Stepper;
@synthesize wave2Word28Stepper = _wave2Word28Stepper;
@synthesize wave2Word29Stepper = _wave2Word29Stepper;
@synthesize wave2Word30Stepper = _wave2Word30Stepper;
@synthesize wave2Word31Stepper = _wave2Word31Stepper;
@synthesize wave2Word32Stepper = _wave2Word32Stepper;

@synthesize wave3SineButton = _wave3SineButton;
@synthesize wave3SquareButton = _wave3SquareButton;
@synthesize wave3TriangleButton = _wave3TriangleButton;
@synthesize wave3SawtoothButton = _wave3SawtoothButton;

@synthesize wave3Word1Slider = _wave3Word1Slider;
@synthesize wave3Word2Slider = _wave3Word2Slider;
@synthesize wave3Word3Slider = _wave3Word3Slider;
@synthesize wave3Word4Slider = _wave3Word4Slider;
@synthesize wave3Word5Slider = _wave3Word5Slider;
@synthesize wave3Word6Slider = _wave3Word6Slider;
@synthesize wave3Word7Slider = _wave3Word7Slider;
@synthesize wave3Word8Slider = _wave3Word8Slider;
@synthesize wave3Word9Slider = _wave3Word9Slider;
@synthesize wave3Word10Slider = _wave3Word10Slider;
@synthesize wave3Word11Slider = _wave3Word11Slider;
@synthesize wave3Word12Slider = _wave3Word12Slider;
@synthesize wave3Word13Slider = _wave3Word13Slider;
@synthesize wave3Word14Slider = _wave3Word14Slider;
@synthesize wave3Word15Slider = _wave3Word15Slider;
@synthesize wave3Word16Slider = _wave3Word16Slider;
@synthesize wave3Word17Slider = _wave3Word17Slider;
@synthesize wave3Word18Slider = _wave3Word18Slider;
@synthesize wave3Word19Slider = _wave3Word19Slider;
@synthesize wave3Word20Slider = _wave3Word20Slider;
@synthesize wave3Word21Slider = _wave3Word21Slider;
@synthesize wave3Word22Slider = _wave3Word22Slider;
@synthesize wave3Word23Slider = _wave3Word23Slider;
@synthesize wave3Word24Slider = _wave3Word24Slider;
@synthesize wave3Word25Slider = _wave3Word25Slider;
@synthesize wave3Word26Slider = _wave3Word26Slider;
@synthesize wave3Word27Slider = _wave3Word27Slider;
@synthesize wave3Word28Slider = _wave3Word28Slider;
@synthesize wave3Word29Slider = _wave3Word29Slider;
@synthesize wave3Word30Slider = _wave3Word30Slider;
@synthesize wave3Word31Slider = _wave3Word31Slider;
@synthesize wave3Word32Slider = _wave3Word32Slider;

@synthesize wave3Word1Stepper = _wave3Word1Stepper;
@synthesize wave3Word2Stepper = _wave3Word2Stepper;
@synthesize wave3Word3Stepper = _wave3Word3Stepper;
@synthesize wave3Word4Stepper = _wave3Word4Stepper;
@synthesize wave3Word5Stepper = _wave3Word5Stepper;
@synthesize wave3Word6Stepper = _wave3Word6Stepper;
@synthesize wave3Word7Stepper = _wave3Word7Stepper;
@synthesize wave3Word8Stepper = _wave3Word8Stepper;
@synthesize wave3Word9Stepper = _wave3Word9Stepper;
@synthesize wave3Word10Stepper = _wave3Word10Stepper;
@synthesize wave3Word11Stepper = _wave3Word11Stepper;
@synthesize wave3Word12Stepper = _wave3Word12Stepper;
@synthesize wave3Word13Stepper = _wave3Word13Stepper;
@synthesize wave3Word14Stepper = _wave3Word14Stepper;
@synthesize wave3Word15Stepper = _wave3Word15Stepper;
@synthesize wave3Word16Stepper = _wave3Word16Stepper;
@synthesize wave3Word17Stepper = _wave3Word17Stepper;
@synthesize wave3Word18Stepper = _wave3Word18Stepper;
@synthesize wave3Word19Stepper = _wave3Word19Stepper;
@synthesize wave3Word20Stepper = _wave3Word20Stepper;
@synthesize wave3Word21Stepper = _wave3Word21Stepper;
@synthesize wave3Word22Stepper = _wave3Word22Stepper;
@synthesize wave3Word23Stepper = _wave3Word23Stepper;
@synthesize wave3Word24Stepper = _wave3Word24Stepper;
@synthesize wave3Word25Stepper = _wave3Word25Stepper;
@synthesize wave3Word26Stepper = _wave3Word26Stepper;
@synthesize wave3Word27Stepper = _wave3Word27Stepper;
@synthesize wave3Word28Stepper = _wave3Word28Stepper;
@synthesize wave3Word29Stepper = _wave3Word29Stepper;
@synthesize wave3Word30Stepper = _wave3Word30Stepper;
@synthesize wave3Word31Stepper = _wave3Word31Stepper;
@synthesize wave3Word32Stepper = _wave3Word32Stepper;

@synthesize wave4SineButton = _wave4SineButton;
@synthesize wave4SquareButton = _wave4SquareButton;
@synthesize wave4TriangleButton = _wave4TriangleButton;
@synthesize wave4SawtoothButton = _wave4SawtoothButton;

@synthesize wave4Word1Slider = _wave4Word1Slider;
@synthesize wave4Word2Slider = _wave4Word2Slider;
@synthesize wave4Word3Slider = _wave4Word3Slider;
@synthesize wave4Word4Slider = _wave4Word4Slider;
@synthesize wave4Word5Slider = _wave4Word5Slider;
@synthesize wave4Word6Slider = _wave4Word6Slider;
@synthesize wave4Word7Slider = _wave4Word7Slider;
@synthesize wave4Word8Slider = _wave4Word8Slider;
@synthesize wave4Word9Slider = _wave4Word9Slider;
@synthesize wave4Word10Slider = _wave4Word10Slider;
@synthesize wave4Word11Slider = _wave4Word11Slider;
@synthesize wave4Word12Slider = _wave4Word12Slider;
@synthesize wave4Word13Slider = _wave4Word13Slider;
@synthesize wave4Word14Slider = _wave4Word14Slider;
@synthesize wave4Word15Slider = _wave4Word15Slider;
@synthesize wave4Word16Slider = _wave4Word16Slider;
@synthesize wave4Word17Slider = _wave4Word17Slider;
@synthesize wave4Word18Slider = _wave4Word18Slider;
@synthesize wave4Word19Slider = _wave4Word19Slider;
@synthesize wave4Word20Slider = _wave4Word20Slider;
@synthesize wave4Word21Slider = _wave4Word21Slider;
@synthesize wave4Word22Slider = _wave4Word22Slider;
@synthesize wave4Word23Slider = _wave4Word23Slider;
@synthesize wave4Word24Slider = _wave4Word24Slider;
@synthesize wave4Word25Slider = _wave4Word25Slider;
@synthesize wave4Word26Slider = _wave4Word26Slider;
@synthesize wave4Word27Slider = _wave4Word27Slider;
@synthesize wave4Word28Slider = _wave4Word28Slider;
@synthesize wave4Word29Slider = _wave4Word29Slider;
@synthesize wave4Word30Slider = _wave4Word30Slider;
@synthesize wave4Word31Slider = _wave4Word31Slider;
@synthesize wave4Word32Slider = _wave4Word32Slider;

@synthesize wave4Word1Stepper = _wave4Word1Stepper;
@synthesize wave4Word2Stepper = _wave4Word2Stepper;
@synthesize wave4Word3Stepper = _wave4Word3Stepper;
@synthesize wave4Word4Stepper = _wave4Word4Stepper;
@synthesize wave4Word5Stepper = _wave4Word5Stepper;
@synthesize wave4Word6Stepper = _wave4Word6Stepper;
@synthesize wave4Word7Stepper = _wave4Word7Stepper;
@synthesize wave4Word8Stepper = _wave4Word8Stepper;
@synthesize wave4Word9Stepper = _wave4Word9Stepper;
@synthesize wave4Word10Stepper = _wave4Word10Stepper;
@synthesize wave4Word11Stepper = _wave4Word11Stepper;
@synthesize wave4Word12Stepper = _wave4Word12Stepper;
@synthesize wave4Word13Stepper = _wave4Word13Stepper;
@synthesize wave4Word14Stepper = _wave4Word14Stepper;
@synthesize wave4Word15Stepper = _wave4Word15Stepper;
@synthesize wave4Word16Stepper = _wave4Word16Stepper;
@synthesize wave4Word17Stepper = _wave4Word17Stepper;
@synthesize wave4Word18Stepper = _wave4Word18Stepper;
@synthesize wave4Word19Stepper = _wave4Word19Stepper;
@synthesize wave4Word20Stepper = _wave4Word20Stepper;
@synthesize wave4Word21Stepper = _wave4Word21Stepper;
@synthesize wave4Word22Stepper = _wave4Word22Stepper;
@synthesize wave4Word23Stepper = _wave4Word23Stepper;
@synthesize wave4Word24Stepper = _wave4Word24Stepper;
@synthesize wave4Word25Stepper = _wave4Word25Stepper;
@synthesize wave4Word26Stepper = _wave4Word26Stepper;
@synthesize wave4Word27Stepper = _wave4Word27Stepper;
@synthesize wave4Word28Stepper = _wave4Word28Stepper;
@synthesize wave4Word29Stepper = _wave4Word29Stepper;
@synthesize wave4Word30Stepper = _wave4Word30Stepper;
@synthesize wave4Word31Stepper = _wave4Word31Stepper;
@synthesize wave4Word32Stepper = _wave4Word32Stepper;

@synthesize wave5SineButton = _wave5SineButton;
@synthesize wave5SquareButton = _wave5SquareButton;
@synthesize wave5TriangleButton = _wave5TriangleButton;
@synthesize wave5SawtoothButton = _wave5SawtoothButton;

@synthesize wave5Word1Slider = _wave5Word1Slider;
@synthesize wave5Word2Slider = _wave5Word2Slider;
@synthesize wave5Word3Slider = _wave5Word3Slider;
@synthesize wave5Word4Slider = _wave5Word4Slider;
@synthesize wave5Word5Slider = _wave5Word5Slider;
@synthesize wave5Word6Slider = _wave5Word6Slider;
@synthesize wave5Word7Slider = _wave5Word7Slider;
@synthesize wave5Word8Slider = _wave5Word8Slider;
@synthesize wave5Word9Slider = _wave5Word9Slider;
@synthesize wave5Word10Slider = _wave5Word10Slider;
@synthesize wave5Word11Slider = _wave5Word11Slider;
@synthesize wave5Word12Slider = _wave5Word12Slider;
@synthesize wave5Word13Slider = _wave5Word13Slider;
@synthesize wave5Word14Slider = _wave5Word14Slider;
@synthesize wave5Word15Slider = _wave5Word15Slider;
@synthesize wave5Word16Slider = _wave5Word16Slider;
@synthesize wave5Word17Slider = _wave5Word17Slider;
@synthesize wave5Word18Slider = _wave5Word18Slider;
@synthesize wave5Word19Slider = _wave5Word19Slider;
@synthesize wave5Word20Slider = _wave5Word20Slider;
@synthesize wave5Word21Slider = _wave5Word21Slider;
@synthesize wave5Word22Slider = _wave5Word22Slider;
@synthesize wave5Word23Slider = _wave5Word23Slider;
@synthesize wave5Word24Slider = _wave5Word24Slider;
@synthesize wave5Word25Slider = _wave5Word25Slider;
@synthesize wave5Word26Slider = _wave5Word26Slider;
@synthesize wave5Word27Slider = _wave5Word27Slider;
@synthesize wave5Word28Slider = _wave5Word28Slider;
@synthesize wave5Word29Slider = _wave5Word29Slider;
@synthesize wave5Word30Slider = _wave5Word30Slider;
@synthesize wave5Word31Slider = _wave5Word31Slider;
@synthesize wave5Word32Slider = _wave5Word32Slider;

@synthesize wave5Word1Stepper = _wave5Word1Stepper;
@synthesize wave5Word2Stepper = _wave5Word2Stepper;
@synthesize wave5Word3Stepper = _wave5Word3Stepper;
@synthesize wave5Word4Stepper = _wave5Word4Stepper;
@synthesize wave5Word5Stepper = _wave5Word5Stepper;
@synthesize wave5Word6Stepper = _wave5Word6Stepper;
@synthesize wave5Word7Stepper = _wave5Word7Stepper;
@synthesize wave5Word8Stepper = _wave5Word8Stepper;
@synthesize wave5Word9Stepper = _wave5Word9Stepper;
@synthesize wave5Word10Stepper = _wave5Word10Stepper;
@synthesize wave5Word11Stepper = _wave5Word11Stepper;
@synthesize wave5Word12Stepper = _wave5Word12Stepper;
@synthesize wave5Word13Stepper = _wave5Word13Stepper;
@synthesize wave5Word14Stepper = _wave5Word14Stepper;
@synthesize wave5Word15Stepper = _wave5Word15Stepper;
@synthesize wave5Word16Stepper = _wave5Word16Stepper;
@synthesize wave5Word17Stepper = _wave5Word17Stepper;
@synthesize wave5Word18Stepper = _wave5Word18Stepper;
@synthesize wave5Word19Stepper = _wave5Word19Stepper;
@synthesize wave5Word20Stepper = _wave5Word20Stepper;
@synthesize wave5Word21Stepper = _wave5Word21Stepper;
@synthesize wave5Word22Stepper = _wave5Word22Stepper;
@synthesize wave5Word23Stepper = _wave5Word23Stepper;
@synthesize wave5Word24Stepper = _wave5Word24Stepper;
@synthesize wave5Word25Stepper = _wave5Word25Stepper;
@synthesize wave5Word26Stepper = _wave5Word26Stepper;
@synthesize wave5Word27Stepper = _wave5Word27Stepper;
@synthesize wave5Word28Stepper = _wave5Word28Stepper;
@synthesize wave5Word29Stepper = _wave5Word29Stepper;
@synthesize wave5Word30Stepper = _wave5Word30Stepper;
@synthesize wave5Word31Stepper = _wave5Word31Stepper;
@synthesize wave5Word32Stepper = _wave5Word32Stepper;

@synthesize modSineButton = _modSineButton;
@synthesize modSquareButton = _modSquareButton;
@synthesize modTriangleButton = _modTriangleButton;
@synthesize modSawtoothButton = _modSawtoothButton;

@synthesize modWord1Slider = _modWord1Slider;
@synthesize modWord2Slider = _modWord2Slider;
@synthesize modWord3Slider = _modWord3Slider;
@synthesize modWord4Slider = _modWord4Slider;
@synthesize modWord5Slider = _modWord5Slider;
@synthesize modWord6Slider = _modWord6Slider;
@synthesize modWord7Slider = _modWord7Slider;
@synthesize modWord8Slider = _modWord8Slider;
@synthesize modWord9Slider = _modWord9Slider;
@synthesize modWord10Slider = _modWord10Slider;
@synthesize modWord11Slider = _modWord11Slider;
@synthesize modWord12Slider = _modWord12Slider;
@synthesize modWord13Slider = _modWord13Slider;
@synthesize modWord14Slider = _modWord14Slider;
@synthesize modWord15Slider = _modWord15Slider;
@synthesize modWord16Slider = _modWord16Slider;
@synthesize modWord17Slider = _modWord17Slider;
@synthesize modWord18Slider = _modWord18Slider;
@synthesize modWord19Slider = _modWord19Slider;
@synthesize modWord20Slider = _modWord20Slider;
@synthesize modWord21Slider = _modWord21Slider;
@synthesize modWord22Slider = _modWord22Slider;
@synthesize modWord23Slider = _modWord23Slider;
@synthesize modWord24Slider = _modWord24Slider;
@synthesize modWord25Slider = _modWord25Slider;
@synthesize modWord26Slider = _modWord26Slider;
@synthesize modWord27Slider = _modWord27Slider;
@synthesize modWord28Slider = _modWord28Slider;
@synthesize modWord29Slider = _modWord29Slider;
@synthesize modWord30Slider = _modWord30Slider;
@synthesize modWord31Slider = _modWord31Slider;
@synthesize modWord32Slider = _modWord32Slider;

@synthesize modWord1Stepper = _modWord1Stepper;
@synthesize modWord2Stepper = _modWord2Stepper;
@synthesize modWord3Stepper = _modWord3Stepper;
@synthesize modWord4Stepper = _modWord4Stepper;
@synthesize modWord5Stepper = _modWord5Stepper;
@synthesize modWord6Stepper = _modWord6Stepper;
@synthesize modWord7Stepper = _modWord7Stepper;
@synthesize modWord8Stepper = _modWord8Stepper;
@synthesize modWord9Stepper = _modWord9Stepper;
@synthesize modWord10Stepper = _modWord10Stepper;
@synthesize modWord11Stepper = _modWord11Stepper;
@synthesize modWord12Stepper = _modWord12Stepper;
@synthesize modWord13Stepper = _modWord13Stepper;
@synthesize modWord14Stepper = _modWord14Stepper;
@synthesize modWord15Stepper = _modWord15Stepper;
@synthesize modWord16Stepper = _modWord16Stepper;
@synthesize modWord17Stepper = _modWord17Stepper;
@synthesize modWord18Stepper = _modWord18Stepper;
@synthesize modWord19Stepper = _modWord19Stepper;
@synthesize modWord20Stepper = _modWord20Stepper;
@synthesize modWord21Stepper = _modWord21Stepper;
@synthesize modWord22Stepper = _modWord22Stepper;
@synthesize modWord23Stepper = _modWord23Stepper;
@synthesize modWord24Stepper = _modWord24Stepper;
@synthesize modWord25Stepper = _modWord25Stepper;
@synthesize modWord26Stepper = _modWord26Stepper;
@synthesize modWord27Stepper = _modWord27Stepper;
@synthesize modWord28Stepper = _modWord28Stepper;
@synthesize modWord29Stepper = _modWord29Stepper;
@synthesize modWord30Stepper = _modWord30Stepper;
@synthesize modWord31Stepper = _modWord31Stepper;
@synthesize modWord32Stepper = _modWord32Stepper;

@synthesize ch1EnabledCheckBox = _ch1EnabledCheckBox;
@synthesize ch1XTextField = _ch1XTextField;
@synthesize ch1HzTextField = _ch1HzTextField;
@synthesize ch1WaveRAMPopUp = _ch1WaveRAMPopUp;
@synthesize ch1IntervalCheckBox = _ch1IntervalCheckBox;
@synthesize ch1IntervalPopUp = _ch1IntervalPopUp;
@synthesize ch1LeftVolSlider = _ch1LeftVolSlider;
@synthesize ch1RightVolSlider = _ch1RightVolSlider;

@synthesize ch1EnvEnabledCheckBox = _ch1EnvEnabledCheckBox;
@synthesize ch1EnvRepeatingCheckBox = _ch1EnvRepeatingCheckBox;
@synthesize ch1EnvDirectionRadios = _ch1EnvDirectionRadios;
@synthesize ch1EnvStepTimePopUp = _ch1EnvStepTimePopUp;
@synthesize ch1EnvInitialValSlider = _ch1EnvInitialValSlider;

@synthesize s1intTextField = _s1intTextField;
@synthesize s1lrvTextField = _s1lrvTextField;
@synthesize s1fqhTextField = _s1fqhTextField;
@synthesize s1fqlTextField = _s1fqlTextField;
@synthesize s1ev0TextField = _s1ev0TextField;
@synthesize s1ev1TextField = _s1ev1TextField;
@synthesize s1ramTextField = _s1ramTextField;

@synthesize ch2EnabledCheckBox = _ch2EnabledCheckBox;
@synthesize ch2XTextField = _ch2XTextField;
@synthesize ch2HzTextField = _ch2HzTextField;
@synthesize ch2WaveRAMPopUp = _ch2WaveRAMPopUp;
@synthesize ch2IntervalCheckBox = _ch2IntervalCheckBox;
@synthesize ch2IntervalPopUp = _ch2IntervalPopUp;
@synthesize ch2LeftVolSlider = _ch2LeftVolSlider;
@synthesize ch2RightVolSlider = _ch2RightVolSlider;

@synthesize ch2EnvEnabledCheckBox = _ch2EnvEnabledCheckBox;
@synthesize ch2EnvRepeatingCheckBox = _ch2EnvRepeatingCheckBox;
@synthesize ch2EnvDirectionRadios = _ch2EnvDirectionRadios;
@synthesize ch2EnvStepTimePopUp = _ch2EnvStepTimePopUp;
@synthesize ch2EnvInitialValSlider = _ch2EnvInitialValSlider;

@synthesize s2intTextField = _s2intTextField;
@synthesize s2lrvTextField = _s2lrvTextField;
@synthesize s2fqhTextField = _s2fqhTextField;
@synthesize s2fqlTextField = _s2fqlTextField;
@synthesize s2ev0TextField = _s2ev0TextField;
@synthesize s2ev1TextField = _s2ev1TextField;
@synthesize s2ramTextField = _s2ramTextField;

@synthesize ch3EnabledCheckBox = _ch3EnabledCheckBox;
@synthesize ch3XTextField = _ch3XTextField;
@synthesize ch3HzTextField = _ch3HzTextField;
@synthesize ch3WaveRAMPopUp = _ch3WaveRAMPopUp;
@synthesize ch3IntervalCheckBox = _ch3IntervalCheckBox;
@synthesize ch3IntervalPopUp = _ch3IntervalPopUp;
@synthesize ch3LeftVolSlider = _ch3LeftVolSlider;
@synthesize ch3RightVolSlider = _ch3RightVolSlider;

@synthesize ch3EnvEnabledCheckBox = _ch3EnvEnabledCheckBox;
@synthesize ch3EnvRepeatingCheckBox = _ch3EnvRepeatingCheckBox;
@synthesize ch3EnvDirectionRadios = _ch3EnvDirectionRadios;
@synthesize ch3EnvStepTimePopUp = _ch3EnvStepTimePopUp;
@synthesize ch3EnvInitialValSlider = _ch3EnvInitialValSlider;

@synthesize s3intTextField = _s3intTextField;
@synthesize s3lrvTextField = _s3lrvTextField;
@synthesize s3fqhTextField = _s3fqhTextField;
@synthesize s3fqlTextField = _s3fqlTextField;
@synthesize s3ev0TextField = _s3ev0TextField;
@synthesize s3ev1TextField = _s3ev1TextField;
@synthesize s3ramTextField = _s3ramTextField;

@synthesize ch4EnabledCheckBox = _ch4EnabledCheckBox;
@synthesize ch4XTextField = _ch4XTextField;
@synthesize ch4HzTextField = _ch4HzTextField;
@synthesize ch4WaveRAMPopUp = _ch4WaveRAMPopUp;
@synthesize ch4IntervalCheckBox = _ch4IntervalCheckBox;
@synthesize ch4IntervalPopUp = _ch4IntervalPopUp;
@synthesize ch4LeftVolSlider = _ch4LeftVolSlider;
@synthesize ch4RightVolSlider = _ch4RightVolSlider;

@synthesize ch4EnvEnabledCheckBox = _ch4EnvEnabledCheckBox;
@synthesize ch4EnvRepeatingCheckBox = _ch4EnvRepeatingCheckBox;
@synthesize ch4EnvDirectionRadios = _ch4EnvDirectionRadios;
@synthesize ch4EnvStepTimePopUp = _ch4EnvStepTimePopUp;
@synthesize ch4EnvInitialValSlider = _ch4EnvInitialValSlider;

@synthesize s4intTextField = _s4intTextField;
@synthesize s4lrvTextField = _s4lrvTextField;
@synthesize s4fqhTextField = _s4fqhTextField;
@synthesize s4fqlTextField = _s4fqlTextField;
@synthesize s4ev0TextField = _s4ev0TextField;
@synthesize s4ev1TextField = _s4ev1TextField;
@synthesize s4ramTextField = _s4ramTextField;

@synthesize ch5EnabledCheckBox = _ch5EnabledCheckBox;
@synthesize ch5XTextField = _ch5XTextField;
@synthesize ch5HzTextField = _ch5HzTextField;
@synthesize ch5WaveRAMPopUp = _ch5WaveRAMPopUp;
@synthesize ch5IntervalCheckBox = _ch5IntervalCheckBox;
@synthesize ch5IntervalPopUp = _ch5IntervalPopUp;
@synthesize ch5LeftVolSlider = _ch5LeftVolSlider;
@synthesize ch5RightVolSlider = _ch5RightVolSlider;

@synthesize ch5EnvEnabledCheckBox = _ch5EnvEnabledCheckBox;
@synthesize ch5EnvRepeatingCheckBox = _ch5EnvRepeatingCheckBox;
@synthesize ch5EnvDirectionRadios = _ch5EnvDirectionRadios;
@synthesize ch5EnvStepTimePopUp = _ch5EnvStepTimePopUp;
@synthesize ch5EnvInitialValSlider = _ch5EnvInitialValSlider;

@synthesize ch5SweepEnabledCheckBox = _ch5SweepEnabledCheckBox;
@synthesize ch5SweepRepeatingCheckBox = _ch5SweepRepeatingCheckBox;
@synthesize ch5SweepFunctionRadios = _ch5SweepFunctionRadios;
@synthesize ch5SweepDirectionRadios = _ch5SweepDirectionRadios;
@synthesize ch5SweepFreqPopUp = _ch5SweepFreqPopUp;
@synthesize ch5SweepIntervalPopUp = _ch5SweepIntervalPopUp;
@synthesize ch5SweepShiftSlider = _ch5SweepShiftSlider;

@synthesize s5intTextField = _s5intTextField;
@synthesize s5lrvTextField = _s5lrvTextField;
@synthesize s5fqhTextField = _s5fqhTextField;
@synthesize s5fqlTextField = _s5fqlTextField;
@synthesize s5ev0TextField = _s5ev0TextField;
@synthesize s5ev1TextField = _s5ev1TextField;
@synthesize s5ramTextField = _s5ramTextField;
@synthesize s5swpTextField = _s5swpTextField;

@synthesize ch6EnabledCheckBox = _ch6EnabledCheckBox;
@synthesize ch6XTextField = _ch6XTextField;
@synthesize ch6HzTextField = _ch6HzTextField;
@synthesize ch6TapLocationPopUp = _ch6TapLocationPopUp;
@synthesize ch6IntervalCheckBox = _ch6IntervalCheckBox;
@synthesize ch6IntervalPopUp = _ch6IntervalPopUp;
@synthesize ch6LeftVolSlider = _ch6LeftVolSlider;
@synthesize ch6RightVolSlider = _ch6RightVolSlider;

@synthesize ch6EnvEnabledCheckBox = _ch6EnvEnabledCheckBox;
@synthesize ch6EnvRepeatingCheckBox = _ch6EnvRepeatingCheckBox;
@synthesize ch6EnvDirectionRadios = _ch6EnvDirectionRadios;
@synthesize ch6EnvStepTimePopUp = _ch6EnvStepTimePopUp;
@synthesize ch6EnvInitialValSlider = _ch6EnvInitialValSlider;

@synthesize s6intTextField = _s6intTextField;
@synthesize s6lrvTextField = _s6lrvTextField;
@synthesize s6fqhTextField = _s6fqhTextField;
@synthesize s6fqlTextField = _s6fqlTextField;
@synthesize s6ev0TextField = _s6ev0TextField;
@synthesize s6ev1TextField = _s6ev1TextField;

@synthesize audioEngine;

- (void)fillInIntRegFieldForChannel:(NSInteger)ch {
    static NSTextField* const intTextFields[6] = {
        self.s1intTextField,
        self.s2intTextField,
        self.s3intTextField,
        self.s4intTextField,
        self.s5intTextField,
        self.s6intTextField
    };
    assert(ch >= 0 && ch < NUM_VSU_CHANNELS);
    NSTextField* intTextField = intTextFields[ch];
    [intTextField setStringValue:[NSString stringWithFormat:@"0x%02X", vsu.sxint[ch]]];
    [self.audioEngine update:vsu];
}

- (void)fillInLRVRegFieldForChannel:(NSInteger)ch {
    static NSTextField* const lrvTextFields[6] = {
        self.s1lrvTextField,
        self.s2lrvTextField,
        self.s3lrvTextField,
        self.s4lrvTextField,
        self.s5lrvTextField,
        self.s6lrvTextField
    };
    assert(ch >= 0 && ch < NUM_VSU_CHANNELS);
    NSTextField* lrvTextField = lrvTextFields[ch];
    [lrvTextField setStringValue:[NSString stringWithFormat:@"0x%02X", vsu.sxlrv[ch]]];
    [self.audioEngine update:vsu];
}

- (void)fillInFQHRegFieldForChannel:(NSInteger)ch {
    static NSTextField* const fqhTextFields[6] = {
        self.s1fqhTextField,
        self.s2fqhTextField,
        self.s3fqhTextField,
        self.s4fqhTextField,
        self.s5fqhTextField,
        self.s6fqhTextField
    };
    assert(ch >= 0 && ch < NUM_VSU_CHANNELS);
    NSTextField* fqhTextField = fqhTextFields[ch];
    [fqhTextField setStringValue:[NSString stringWithFormat:@"0x%02X", vsu.sxfqh[ch]]];
    [self.audioEngine update:vsu];
}

- (void)fillInFQLRegFieldForChannel:(NSInteger)ch {
    static NSTextField* const fqlTextFields[6] = {
        self.s1fqlTextField,
        self.s2fqlTextField,
        self.s3fqlTextField,
        self.s4fqlTextField,
        self.s5fqlTextField,
        self.s6fqlTextField
    };
    assert(ch >= 0 && ch < NUM_VSU_CHANNELS);
    NSTextField* fqlTextField = fqlTextFields[ch];
    [fqlTextField setStringValue:[NSString stringWithFormat:@"0x%02X", vsu.sxfql[ch]]];
    [self.audioEngine update:vsu];
}

- (void)fillInEV0RegFieldForChannel:(NSInteger)ch {
    static NSTextField* const ev0TextFields[6] = {
        self.s1ev0TextField,
        self.s2ev0TextField,
        self.s3ev0TextField,
        self.s4ev0TextField,
        self.s5ev0TextField,
        self.s6ev0TextField
    };
    assert(ch >= 0 && ch < NUM_VSU_CHANNELS);
    NSTextField* ev0TextField = ev0TextFields[ch];
    [ev0TextField setStringValue:[NSString stringWithFormat:@"0x%02X", vsu.sxev0[ch]]];
    [self.audioEngine update:vsu];
}

- (void)fillInEV1RegFieldForChannel:(NSInteger)ch {
    static NSTextField* const ev1TextFields[6] = {
        self.s1ev1TextField,
        self.s2ev1TextField,
        self.s3ev1TextField,
        self.s4ev1TextField,
        self.s5ev1TextField,
        self.s6ev1TextField
    };
    assert(ch >= 0 && ch < NUM_VSU_CHANNELS);
    NSTextField* ev1TextField = ev1TextFields[ch];
    [ev1TextField setStringValue:[NSString stringWithFormat:@"0x%02X", vsu.sxev1[ch]]];
    [self.audioEngine update:vsu];
}

- (void)fillInRAMRegFieldForChannel:(NSInteger)ch {
    static NSTextField* const ramTextFields[5] = {
        self.s1ramTextField,
        self.s2ramTextField,
        self.s3ramTextField,
        self.s4ramTextField,
        self.s5ramTextField
    };
    assert(ch >= 0 && ch < NUM_VSU_CHANNELS);
    if (ch < NUM_VSU_CHANNELS - 1)
    {
        NSTextField* ramTextField = ramTextFields[ch];
        [ramTextField setStringValue:[NSString stringWithFormat:@"0x%02X", vsu.sxram[ch]]];
        [self.audioEngine update:vsu];
    }
}

- (void)fillInSWPRegFieldForChannel:(NSInteger)ch {
    assert(ch >= 0 && ch < NUM_VSU_CHANNELS);
    if (ch == 4)
    {
        [self.s5swpTextField setStringValue:[NSString stringWithFormat:@"0x%02X", vsu.s5swp]];
        [self.audioEngine update:vsu];
    }
}

- (void)fillInFreqFieldsForChannel:(NSInteger)ch {
    static const struct
    {
        NSTextField* const x;
        NSTextField* const hz;
    } textFields[6] = {
        {self.ch1XTextField, self.ch1HzTextField},
        {self.ch2XTextField, self.ch2HzTextField},
        {self.ch3XTextField, self.ch3HzTextField},
        {self.ch4XTextField, self.ch4HzTextField},
        {self.ch5XTextField, self.ch5HzTextField},
        {self.ch6XTextField, self.ch6HzTextField}
    };
    assert(ch >= 0 && ch < NUM_VSU_CHANNELS);
    NSTextField* xTextField = textFields[ch].x;
    NSTextField* hzTextField = textFields[ch].hz;
    int x = xFromFQHLReg(vsu.sxfqh[ch], vsu.sxfql[ch]);
    [xTextField setStringValue:[NSString stringWithFormat:@"%u", x]];
    if (ch != 5)
    {
        [hzTextField setStringValue:[NSString stringWithFormat:@"%1.3f", computeFreqFromX(x)]];        
    }
    else
    {
        [hzTextField setStringValue:[NSString stringWithFormat:@"%1.3f", computeNoiseFreqFromX(x)]];  
    }
    [self.audioEngine update:vsu];
}

- (void)updateUIFromWord:(NSInteger)word forWave:(NSInteger)wave {
    static const struct
    {
        NSSlider* const slider;
        NSStepper* const stepper;
    } waveWords[6][32] = {{
        {self.wave1Word1Slider, self.wave1Word1Stepper},
        {self.wave1Word2Slider, self.wave1Word2Stepper},
        {self.wave1Word3Slider, self.wave1Word3Stepper},
        {self.wave1Word4Slider, self.wave1Word4Stepper},
        {self.wave1Word5Slider, self.wave1Word5Stepper},
        {self.wave1Word6Slider, self.wave1Word6Stepper},
        {self.wave1Word7Slider, self.wave1Word7Stepper},
        {self.wave1Word8Slider, self.wave1Word8Stepper},
        {self.wave1Word9Slider, self.wave1Word9Stepper},
        {self.wave1Word10Slider, self.wave1Word10Stepper},
        {self.wave1Word11Slider, self.wave1Word11Stepper},
        {self.wave1Word12Slider, self.wave1Word12Stepper},
        {self.wave1Word13Slider, self.wave1Word13Stepper},
        {self.wave1Word14Slider, self.wave1Word14Stepper},
        {self.wave1Word15Slider, self.wave1Word15Stepper},
        {self.wave1Word16Slider, self.wave1Word16Stepper},
        {self.wave1Word17Slider, self.wave1Word17Stepper},
        {self.wave1Word18Slider, self.wave1Word18Stepper},
        {self.wave1Word19Slider, self.wave1Word19Stepper},
        {self.wave1Word20Slider, self.wave1Word20Stepper},
        {self.wave1Word21Slider, self.wave1Word21Stepper},
        {self.wave1Word22Slider, self.wave1Word22Stepper},
        {self.wave1Word23Slider, self.wave1Word23Stepper},
        {self.wave1Word24Slider, self.wave1Word24Stepper},
        {self.wave1Word25Slider, self.wave1Word25Stepper},
        {self.wave1Word26Slider, self.wave1Word26Stepper},
        {self.wave1Word27Slider, self.wave1Word27Stepper},
        {self.wave1Word28Slider, self.wave1Word28Stepper},
        {self.wave1Word29Slider, self.wave1Word29Stepper},
        {self.wave1Word30Slider, self.wave1Word30Stepper},
        {self.wave1Word31Slider, self.wave1Word31Stepper},
        {self.wave1Word32Slider, self.wave1Word32Stepper}
    }, {
        {self.wave2Word1Slider, self.wave2Word1Stepper},
        {self.wave2Word2Slider, self.wave2Word2Stepper},
        {self.wave2Word3Slider, self.wave2Word3Stepper},
        {self.wave2Word4Slider, self.wave2Word4Stepper},
        {self.wave2Word5Slider, self.wave2Word5Stepper},
        {self.wave2Word6Slider, self.wave2Word6Stepper},
        {self.wave2Word7Slider, self.wave2Word7Stepper},
        {self.wave2Word8Slider, self.wave2Word8Stepper},
        {self.wave2Word9Slider, self.wave2Word9Stepper},
        {self.wave2Word10Slider, self.wave2Word10Stepper},
        {self.wave2Word11Slider, self.wave2Word11Stepper},
        {self.wave2Word12Slider, self.wave2Word12Stepper},
        {self.wave2Word13Slider, self.wave2Word13Stepper},
        {self.wave2Word14Slider, self.wave2Word14Stepper},
        {self.wave2Word15Slider, self.wave2Word15Stepper},
        {self.wave2Word16Slider, self.wave2Word16Stepper},
        {self.wave2Word17Slider, self.wave2Word17Stepper},
        {self.wave2Word18Slider, self.wave2Word18Stepper},
        {self.wave2Word19Slider, self.wave2Word19Stepper},
        {self.wave2Word20Slider, self.wave2Word20Stepper},
        {self.wave2Word21Slider, self.wave2Word21Stepper},
        {self.wave2Word22Slider, self.wave2Word22Stepper},
        {self.wave2Word23Slider, self.wave2Word23Stepper},
        {self.wave2Word24Slider, self.wave2Word24Stepper},
        {self.wave2Word25Slider, self.wave2Word25Stepper},
        {self.wave2Word26Slider, self.wave2Word26Stepper},
        {self.wave2Word27Slider, self.wave2Word27Stepper},
        {self.wave2Word28Slider, self.wave2Word28Stepper},
        {self.wave2Word29Slider, self.wave2Word29Stepper},
        {self.wave2Word30Slider, self.wave2Word30Stepper},
        {self.wave2Word31Slider, self.wave2Word31Stepper},
        {self.wave2Word32Slider, self.wave2Word32Stepper}
    }, {
        {self.wave3Word1Slider, self.wave3Word1Stepper},
        {self.wave3Word2Slider, self.wave3Word2Stepper},
        {self.wave3Word3Slider, self.wave3Word3Stepper},
        {self.wave3Word4Slider, self.wave3Word4Stepper},
        {self.wave3Word5Slider, self.wave3Word5Stepper},
        {self.wave3Word6Slider, self.wave3Word6Stepper},
        {self.wave3Word7Slider, self.wave3Word7Stepper},
        {self.wave3Word8Slider, self.wave3Word8Stepper},
        {self.wave3Word9Slider, self.wave3Word9Stepper},
        {self.wave3Word10Slider, self.wave3Word10Stepper},
        {self.wave3Word11Slider, self.wave3Word11Stepper},
        {self.wave3Word12Slider, self.wave3Word12Stepper},
        {self.wave3Word13Slider, self.wave3Word13Stepper},
        {self.wave3Word14Slider, self.wave3Word14Stepper},
        {self.wave3Word15Slider, self.wave3Word15Stepper},
        {self.wave3Word16Slider, self.wave3Word16Stepper},
        {self.wave3Word17Slider, self.wave3Word17Stepper},
        {self.wave3Word18Slider, self.wave3Word18Stepper},
        {self.wave3Word19Slider, self.wave3Word19Stepper},
        {self.wave3Word20Slider, self.wave3Word20Stepper},
        {self.wave3Word21Slider, self.wave3Word21Stepper},
        {self.wave3Word22Slider, self.wave3Word22Stepper},
        {self.wave3Word23Slider, self.wave3Word23Stepper},
        {self.wave3Word24Slider, self.wave3Word24Stepper},
        {self.wave3Word25Slider, self.wave3Word25Stepper},
        {self.wave3Word26Slider, self.wave3Word26Stepper},
        {self.wave3Word27Slider, self.wave3Word27Stepper},
        {self.wave3Word28Slider, self.wave3Word28Stepper},
        {self.wave3Word29Slider, self.wave3Word29Stepper},
        {self.wave3Word30Slider, self.wave3Word30Stepper},
        {self.wave3Word31Slider, self.wave3Word31Stepper},
        {self.wave3Word32Slider, self.wave3Word32Stepper}
    }, {
        {self.wave4Word1Slider, self.wave4Word1Stepper},
        {self.wave4Word2Slider, self.wave4Word2Stepper},
        {self.wave4Word3Slider, self.wave4Word3Stepper},
        {self.wave4Word4Slider, self.wave4Word4Stepper},
        {self.wave4Word5Slider, self.wave4Word5Stepper},
        {self.wave4Word6Slider, self.wave4Word6Stepper},
        {self.wave4Word7Slider, self.wave4Word7Stepper},
        {self.wave4Word8Slider, self.wave4Word8Stepper},
        {self.wave4Word9Slider, self.wave4Word9Stepper},
        {self.wave4Word10Slider, self.wave4Word10Stepper},
        {self.wave4Word11Slider, self.wave4Word11Stepper},
        {self.wave4Word12Slider, self.wave4Word12Stepper},
        {self.wave4Word13Slider, self.wave4Word13Stepper},
        {self.wave4Word14Slider, self.wave4Word14Stepper},
        {self.wave4Word15Slider, self.wave4Word15Stepper},
        {self.wave4Word16Slider, self.wave4Word16Stepper},
        {self.wave4Word17Slider, self.wave4Word17Stepper},
        {self.wave4Word18Slider, self.wave4Word18Stepper},
        {self.wave4Word19Slider, self.wave4Word19Stepper},
        {self.wave4Word20Slider, self.wave4Word20Stepper},
        {self.wave4Word21Slider, self.wave4Word21Stepper},
        {self.wave4Word22Slider, self.wave4Word22Stepper},
        {self.wave4Word23Slider, self.wave4Word23Stepper},
        {self.wave4Word24Slider, self.wave4Word24Stepper},
        {self.wave4Word25Slider, self.wave4Word25Stepper},
        {self.wave4Word26Slider, self.wave4Word26Stepper},
        {self.wave4Word27Slider, self.wave4Word27Stepper},
        {self.wave4Word28Slider, self.wave4Word28Stepper},
        {self.wave4Word29Slider, self.wave4Word29Stepper},
        {self.wave4Word30Slider, self.wave4Word30Stepper},
        {self.wave4Word31Slider, self.wave4Word31Stepper},
        {self.wave4Word32Slider, self.wave4Word32Stepper}
    }, {
        {self.wave5Word1Slider, self.wave5Word1Stepper},
        {self.wave5Word2Slider, self.wave5Word2Stepper},
        {self.wave5Word3Slider, self.wave5Word3Stepper},
        {self.wave5Word4Slider, self.wave5Word4Stepper},
        {self.wave5Word5Slider, self.wave5Word5Stepper},
        {self.wave5Word6Slider, self.wave5Word6Stepper},
        {self.wave5Word7Slider, self.wave5Word7Stepper},
        {self.wave5Word8Slider, self.wave5Word8Stepper},
        {self.wave5Word9Slider, self.wave5Word9Stepper},
        {self.wave5Word10Slider, self.wave5Word10Stepper},
        {self.wave5Word11Slider, self.wave5Word11Stepper},
        {self.wave5Word12Slider, self.wave5Word12Stepper},
        {self.wave5Word13Slider, self.wave5Word13Stepper},
        {self.wave5Word14Slider, self.wave5Word14Stepper},
        {self.wave5Word15Slider, self.wave5Word15Stepper},
        {self.wave5Word16Slider, self.wave5Word16Stepper},
        {self.wave5Word17Slider, self.wave5Word17Stepper},
        {self.wave5Word18Slider, self.wave5Word18Stepper},
        {self.wave5Word19Slider, self.wave5Word19Stepper},
        {self.wave5Word20Slider, self.wave5Word20Stepper},
        {self.wave5Word21Slider, self.wave5Word21Stepper},
        {self.wave5Word22Slider, self.wave5Word22Stepper},
        {self.wave5Word23Slider, self.wave5Word23Stepper},
        {self.wave5Word24Slider, self.wave5Word24Stepper},
        {self.wave5Word25Slider, self.wave5Word25Stepper},
        {self.wave5Word26Slider, self.wave5Word26Stepper},
        {self.wave5Word27Slider, self.wave5Word27Stepper},
        {self.wave5Word28Slider, self.wave5Word28Stepper},
        {self.wave5Word29Slider, self.wave5Word29Stepper},
        {self.wave5Word30Slider, self.wave5Word30Stepper},
        {self.wave5Word31Slider, self.wave5Word31Stepper},
        {self.wave5Word32Slider, self.wave5Word32Stepper}
    }, {
        {self.modWord1Slider, self.modWord1Stepper},
        {self.modWord2Slider, self.modWord2Stepper},
        {self.modWord3Slider, self.modWord3Stepper},
        {self.modWord4Slider, self.modWord4Stepper},
        {self.modWord5Slider, self.modWord5Stepper},
        {self.modWord6Slider, self.modWord6Stepper},
        {self.modWord7Slider, self.modWord7Stepper},
        {self.modWord8Slider, self.modWord8Stepper},
        {self.modWord9Slider, self.modWord9Stepper},
        {self.modWord10Slider, self.modWord10Stepper},
        {self.modWord11Slider, self.modWord11Stepper},
        {self.modWord12Slider, self.modWord12Stepper},
        {self.modWord13Slider, self.modWord13Stepper},
        {self.modWord14Slider, self.modWord14Stepper},
        {self.modWord15Slider, self.modWord15Stepper},
        {self.modWord16Slider, self.modWord16Stepper},
        {self.modWord17Slider, self.modWord17Stepper},
        {self.modWord18Slider, self.modWord18Stepper},
        {self.modWord19Slider, self.modWord19Stepper},
        {self.modWord20Slider, self.modWord20Stepper},
        {self.modWord21Slider, self.modWord21Stepper},
        {self.modWord22Slider, self.modWord22Stepper},
        {self.modWord23Slider, self.modWord23Stepper},
        {self.modWord24Slider, self.modWord24Stepper},
        {self.modWord25Slider, self.modWord25Stepper},
        {self.modWord26Slider, self.modWord26Stepper},
        {self.modWord27Slider, self.modWord27Stepper},
        {self.modWord28Slider, self.modWord28Stepper},
        {self.modWord29Slider, self.modWord29Stepper},
        {self.modWord30Slider, self.modWord30Stepper},
        {self.modWord31Slider, self.modWord31Stepper},
        {self.modWord32Slider, self.modWord32Stepper}
    }};
    assert(wave >= 0 && wave < 6);
    assert(word >= 0 && word < 32);
    NSSlider* waveWordSlider = waveWords[wave][word].slider;
    NSStepper* waveWordStepper = waveWords[wave][word].stepper;
    [waveWordSlider setIntValue:waveData(vsu.waveram[wave][word])];
    [waveWordStepper setIntValue:waveData(vsu.waveram[wave][word])];
    [self.audioEngine update:vsu];
}

- (void)updateFreqForChannel:(NSInteger)ch withX:(NSInteger)x {
    assert(ch >= 0 && ch < NUM_VSU_CHANNELS);
    x = MAX(0, MIN(x, 2047));
    vsu.sxfqh[ch] = computeFQHReg((int)x);
    vsu.sxfql[ch] = computeFQLReg((int)x);
    [self fillInFreqFieldsForChannel:ch];
    [self fillInFQHRegFieldForChannel:ch];
    [self fillInFQLRegFieldForChannel:ch];
}

- (void)updateUIFromIntRegForChannel:(NSInteger)ch {
    static const struct {
        NSButton* const enabledCheckBox;
        NSButton* const intervalCheckBox;
        NSPopUpButton* const intervalPopUp;
    } intRegUI[6] = {
        {self.ch1EnabledCheckBox, self.ch1IntervalCheckBox, self.ch1IntervalPopUp},
        {self.ch2EnabledCheckBox, self.ch2IntervalCheckBox, self.ch2IntervalPopUp},
        {self.ch3EnabledCheckBox, self.ch3IntervalCheckBox, self.ch3IntervalPopUp},
        {self.ch4EnabledCheckBox, self.ch4IntervalCheckBox, self.ch4IntervalPopUp},
        {self.ch5EnabledCheckBox, self.ch5IntervalCheckBox, self.ch5IntervalPopUp},
        {self.ch6EnabledCheckBox, self.ch6IntervalCheckBox, self.ch6IntervalPopUp}
    };
    assert(ch >= 0 && ch < NUM_VSU_CHANNELS);
    NSButton* enabledCheckBox = intRegUI[ch].enabledCheckBox;
    NSButton* intervalCheckBox = intRegUI[ch].intervalCheckBox;
    NSPopUpButton* intervalPopUp = intRegUI[ch].intervalPopUp;
    [enabledCheckBox setState:isEnabled(vsu.sxint[ch]) ? NSOnState : NSOffState];
    [intervalCheckBox setState:isIntervalEnabled(vsu.sxint[ch]) ? NSOnState : NSOffState];
    [intervalPopUp selectItemAtIndex:intervalData(vsu.sxint[ch])];
}

- (void)updateUIFromLRVRegForChannel:(NSInteger)ch {
    static const struct {
        NSSlider* const leftVolSlider;
        NSSlider* const rightVolSlider;
    } lrvRegUI[6] = {
        {self.ch1LeftVolSlider, self.ch1RightVolSlider},
        {self.ch2LeftVolSlider, self.ch2RightVolSlider},
        {self.ch3LeftVolSlider, self.ch3RightVolSlider},
        {self.ch4LeftVolSlider, self.ch4RightVolSlider},
        {self.ch5LeftVolSlider, self.ch5RightVolSlider},
        {self.ch6LeftVolSlider, self.ch6RightVolSlider}
    };
    assert(ch >= 0 && ch < NUM_VSU_CHANNELS);
    NSSlider* leftVolSlider = lrvRegUI[ch].leftVolSlider;
    NSSlider* rightVolSlider = lrvRegUI[ch].rightVolSlider;
    [leftVolSlider setIntValue:leftVolume(vsu.sxlrv[ch])];
    [rightVolSlider setIntValue:rightVolume(vsu.sxlrv[ch])];
}

- (void)updateUIFromEV0RegForChannel:(NSInteger)ch {
    static const struct {
        NSSlider* const envInitialValSlider;
        NSMatrix* const envDirectionRadios;
        NSPopUpButton* const envStepTimePopUp;
    } ev0RegUI[6] = {
        {self.ch1EnvInitialValSlider, self.ch1EnvDirectionRadios, self.ch1EnvStepTimePopUp},
        {self.ch2EnvInitialValSlider, self.ch2EnvDirectionRadios, self.ch2EnvStepTimePopUp},
        {self.ch3EnvInitialValSlider, self.ch3EnvDirectionRadios, self.ch3EnvStepTimePopUp},
        {self.ch4EnvInitialValSlider, self.ch4EnvDirectionRadios, self.ch4EnvStepTimePopUp},
        {self.ch5EnvInitialValSlider, self.ch5EnvDirectionRadios, self.ch5EnvStepTimePopUp},
        {self.ch6EnvInitialValSlider, self.ch6EnvDirectionRadios, self.ch6EnvStepTimePopUp}
    };
    assert(ch >= 0 && ch < NUM_VSU_CHANNELS);
    NSSlider* envInitialValSlider = ev0RegUI[ch].envInitialValSlider;
    NSMatrix* envDirectionRadios = ev0RegUI[ch].envDirectionRadios;
    NSPopUpButton* envStepTimePopUp = ev0RegUI[ch].envStepTimePopUp;
    [envInitialValSlider setIntValue:envInitialValue(vsu.sxev0[ch])];
    [envDirectionRadios selectCellAtRow:envDirection(vsu.sxev0[ch]) column:0];
    [envStepTimePopUp selectItemAtIndex:envStepTime(vsu.sxev0[ch])];
}

- (void)updateUIFromEV1RegForChannel:(NSInteger)ch {
    static const struct {
        NSButton* const envEnabledCheckBox;
        NSButton* const envRepeatingCheckBox;
    } ev1RegUI[6] = {
        {self.ch1EnvEnabledCheckBox, self.ch1EnvRepeatingCheckBox},
        {self.ch2EnvEnabledCheckBox, self.ch2EnvRepeatingCheckBox},
        {self.ch3EnvEnabledCheckBox, self.ch3EnvRepeatingCheckBox},
        {self.ch4EnvEnabledCheckBox, self.ch4EnvRepeatingCheckBox},
        {self.ch5EnvEnabledCheckBox, self.ch5EnvRepeatingCheckBox},
        {self.ch6EnvEnabledCheckBox, self.ch6EnvRepeatingCheckBox}
    };
    assert(ch >= 0 && ch < NUM_VSU_CHANNELS);
    NSButton* envEnabledCheckBox = ev1RegUI[ch].envEnabledCheckBox;
    NSButton* envRepeatingCheckBox = ev1RegUI[ch].envRepeatingCheckBox;
    [envEnabledCheckBox setState:isEnvelopeEnabled(vsu.sxev1[ch]) ? NSOnState : NSOffState];
    [envRepeatingCheckBox setState:isEnvelopeRepeating(vsu.sxev1[ch]) ? NSOnState : NSOffState];
    if (ch == 4)
    {
        [self.ch5SweepEnabledCheckBox setState:isSweepEnabled(vsu.sxev1[ch]) ? NSOnState : NSOffState];
        [self.ch5SweepRepeatingCheckBox setState:isSweepRepeating(vsu.sxev1[ch]) ? NSOnState : NSOffState];
        [self.ch5SweepFunctionRadios selectCellAtRow:modulationFunction(vsu.sxev1[ch]) column:0];
    }
}

- (void)updateUIFromRAMRegForChannel:(NSInteger)ch {
    static NSPopUpButton* const waveRAMPopUps[5] = {
        self.ch1WaveRAMPopUp,
        self.ch2WaveRAMPopUp,
        self.ch3WaveRAMPopUp,
        self.ch4WaveRAMPopUp,
        self.ch5WaveRAMPopUp
    };
    assert(ch >= 0 && ch < NUM_VSU_CHANNELS);
    if (ch != 5)
    {
        NSPopUpButton* waveRAMPopUp = waveRAMPopUps[ch];
        [waveRAMPopUp selectItemAtIndex:waveRAM(vsu.sxram[ch])];
    }
}

- (void)updateUIFromSWPRegForChannel:(NSInteger)ch {
    assert(ch >= 0 && ch < NUM_VSU_CHANNELS);
    if (ch == 4)
    {
        [self.ch5SweepFreqPopUp selectItemAtIndex:sweepFreq(vsu.s5swp)];
        [self.ch5SweepIntervalPopUp selectItemAtIndex:sweepInterval(vsu.s5swp)];
        [self.ch5SweepDirectionRadios selectCellAtRow:sweepDirection(vsu.s5swp) column:0];
        [self.ch5SweepShiftSlider setIntValue:sweepShift(vsu.s5swp)];
    }
}

#pragma mark UI controls

- (IBAction)makeSineWave:(id)sender {
    NSInteger wave = [sender tag];
    float range = wave != 5 ? 31.5f : 127.5f;
    for (int i = 0; i < 32; ++i)
    {
        vsu.waveram[wave][i] = computeWaveWord((int)(range * sinf(((M_PI / 16.0f) * i) - M_PI / 2.0f) + range));
        [self updateUIFromWord:i forWave:wave];
    }
}

- (IBAction)makeSquareWave:(id)sender {
    NSInteger wave = [sender tag];
    int range = wave != 5 ? 0x3F : 0xFF;
    for (int i = 0; i < 16; ++i)
    {
        vsu.waveram[wave][i] = computeWaveWord(0x00);
        [self updateUIFromWord:i forWave:wave];
    }
    for (int i = 16; i < 32; ++i)
    {
        vsu.waveram[wave][i] = computeWaveWord(range);
        [self updateUIFromWord:i forWave:wave];
    }
}

- (IBAction)makeTriangleWave:(id)sender {
    NSInteger wave = [sender tag];
    int range = wave != 5 ? 64 : 256;
    vsu.waveram[wave][0] = computeWaveWord(0x00);
    [self updateUIFromWord:0 forWave:wave];
    for (int i = 1; i < 16; ++i)
    {
        vsu.waveram[wave][i] = computeWaveWord((range / 16) * i - 1);
        [self updateUIFromWord:i forWave:wave];
    }
    for (int i = 16; i < 32; ++i)
    {
        vsu.waveram[wave][i] = computeWaveWord(range - (range / 16) * (i - 16) - 1);
        [self updateUIFromWord:i forWave:wave];
    }
}

- (IBAction)makeSawtoothWave:(id)sender {
    NSInteger wave = [sender tag];
    int step = wave != 5 ? 2 : 8;
    for (int i = 0; i < 32; ++i)
    {
        vsu.waveram[wave][i] = computeWaveWord((step / 2) + step * i);
        [self updateUIFromWord:i forWave:wave];
    }
}

- (IBAction)takeIntValueForWaveWord1From:(id)sender {
    NSInteger wave = [sender tag];
    NSInteger word = 0;
    vsu.waveram[wave][word] = computeWaveWord((int)[sender intValue]);
    [self updateUIFromWord:word forWave:wave];
}

- (IBAction)takeIntValueForWaveWord2From:(id)sender {
    NSInteger wave = [sender tag];
    NSInteger word = 1;
    vsu.waveram[wave][word] = computeWaveWord((int)[sender intValue]);
    [self updateUIFromWord:word forWave:wave];
}

- (IBAction)takeIntValueForWaveWord3From:(id)sender {
    NSInteger wave = [sender tag];
    NSInteger word = 2;
    vsu.waveram[wave][word] = computeWaveWord((int)[sender intValue]);
    [self updateUIFromWord:word forWave:wave];
}

- (IBAction)takeIntValueForWaveWord4From:(id)sender {
    NSInteger wave = [sender tag];
    NSInteger word = 3;
    vsu.waveram[wave][word] = computeWaveWord((int)[sender intValue]);
    [self updateUIFromWord:word forWave:wave];
}

- (IBAction)takeIntValueForWaveWord5From:(id)sender {
    NSInteger wave = [sender tag];
    NSInteger word = 4;
    vsu.waveram[wave][word] = computeWaveWord((int)[sender intValue]);
    [self updateUIFromWord:word forWave:wave];
}

- (IBAction)takeIntValueForWaveWord6From:(id)sender {
    NSInteger wave = [sender tag];
    NSInteger word = 5;
    vsu.waveram[wave][word] = computeWaveWord((int)[sender intValue]);
    [self updateUIFromWord:word forWave:wave];
}

- (IBAction)takeIntValueForWaveWord7From:(id)sender {
    NSInteger wave = [sender tag];
    NSInteger word = 6;
    vsu.waveram[wave][word] = computeWaveWord((int)[sender intValue]);
    [self updateUIFromWord:word forWave:wave];
}

- (IBAction)takeIntValueForWaveWord8From:(id)sender {
    NSInteger wave = [sender tag];
    NSInteger word = 7;
    vsu.waveram[wave][word] = computeWaveWord((int)[sender intValue]);
    [self updateUIFromWord:word forWave:wave];
}

- (IBAction)takeIntValueForWaveWord9From:(id)sender {
    NSInteger wave = [sender tag];
    NSInteger word = 8;
    vsu.waveram[wave][word] = computeWaveWord((int)[sender intValue]);
    [self updateUIFromWord:word forWave:wave];
}

- (IBAction)takeIntValueForWaveWord10From:(id)sender {
    NSInteger wave = [sender tag];
    NSInteger word = 9;
    vsu.waveram[wave][word] = computeWaveWord((int)[sender intValue]);
    [self updateUIFromWord:word forWave:wave];
}

- (IBAction)takeIntValueForWaveWord11From:(id)sender {
    NSInteger wave = [sender tag];
    NSInteger word = 10;
    vsu.waveram[wave][word] = computeWaveWord((int)[sender intValue]);
    [self updateUIFromWord:word forWave:wave];
}

- (IBAction)takeIntValueForWaveWord12From:(id)sender {
    NSInteger wave = [sender tag];
    NSInteger word = 11;
    vsu.waveram[wave][word] = computeWaveWord((int)[sender intValue]);
    [self updateUIFromWord:word forWave:wave];
}

- (IBAction)takeIntValueForWaveWord13From:(id)sender {
    NSInteger wave = [sender tag];
    NSInteger word = 12;
    vsu.waveram[wave][word] = computeWaveWord((int)[sender intValue]);
    [self updateUIFromWord:word forWave:wave];
}

- (IBAction)takeIntValueForWaveWord14From:(id)sender {
    NSInteger wave = [sender tag];
    NSInteger word = 13;
    vsu.waveram[wave][word] = computeWaveWord((int)[sender intValue]);
    [self updateUIFromWord:word forWave:wave];
}

- (IBAction)takeIntValueForWaveWord15From:(id)sender {
    NSInteger wave = [sender tag];
    NSInteger word = 14;
    vsu.waveram[wave][word] = computeWaveWord((int)[sender intValue]);
    [self updateUIFromWord:word forWave:wave];
}

- (IBAction)takeIntValueForWaveWord16From:(id)sender {
    NSInteger wave = [sender tag];
    NSInteger word = 15;
    vsu.waveram[wave][word] = computeWaveWord((int)[sender intValue]);
    [self updateUIFromWord:word forWave:wave];
}

- (IBAction)takeIntValueForWaveWord17From:(id)sender {
    NSInteger wave = [sender tag];
    NSInteger word = 16;
    vsu.waveram[wave][word] = computeWaveWord((int)[sender intValue]);
    [self updateUIFromWord:word forWave:wave];
}

- (IBAction)takeIntValueForWaveWord18From:(id)sender {
    NSInteger wave = [sender tag];
    NSInteger word = 17;
    vsu.waveram[wave][word] = computeWaveWord((int)[sender intValue]);
    [self updateUIFromWord:word forWave:wave];
}

- (IBAction)takeIntValueForWaveWord19From:(id)sender {
    NSInteger wave = [sender tag];
    NSInteger word = 18;
    vsu.waveram[wave][word] = computeWaveWord((int)[sender intValue]);
    [self updateUIFromWord:word forWave:wave];
}

- (IBAction)takeIntValueForWaveWord20From:(id)sender {
    NSInteger wave = [sender tag];
    NSInteger word = 19;
    vsu.waveram[wave][word] = computeWaveWord((int)[sender intValue]);
    [self updateUIFromWord:word forWave:wave];
}

- (IBAction)takeIntValueForWaveWord21From:(id)sender {
    NSInteger wave = [sender tag];
    NSInteger word = 20;
    vsu.waveram[wave][word] = computeWaveWord((int)[sender intValue]);
    [self updateUIFromWord:word forWave:wave];
}

- (IBAction)takeIntValueForWaveWord22From:(id)sender {
    NSInteger wave = [sender tag];
    NSInteger word = 21;
    vsu.waveram[wave][word] = computeWaveWord((int)[sender intValue]);
    [self updateUIFromWord:word forWave:wave];
}

- (IBAction)takeIntValueForWaveWord23From:(id)sender {
    NSInteger wave = [sender tag];
    NSInteger word = 22;
    vsu.waveram[wave][word] = computeWaveWord((int)[sender intValue]);
    [self updateUIFromWord:word forWave:wave];
}

- (IBAction)takeIntValueForWaveWord24From:(id)sender {
    NSInteger wave = [sender tag];
    NSInteger word = 23;
    vsu.waveram[wave][word] = computeWaveWord((int)[sender intValue]);
    [self updateUIFromWord:word forWave:wave];
}

- (IBAction)takeIntValueForWaveWord25From:(id)sender {
    NSInteger wave = [sender tag];
    NSInteger word = 24;
    vsu.waveram[wave][word] = computeWaveWord((int)[sender intValue]);
    [self updateUIFromWord:word forWave:wave];
}

- (IBAction)takeIntValueForWaveWord26From:(id)sender {
    NSInteger wave = [sender tag];
    NSInteger word = 25;
    vsu.waveram[wave][word] = computeWaveWord((int)[sender intValue]);
    [self updateUIFromWord:word forWave:wave];
}

- (IBAction)takeIntValueForWaveWord27From:(id)sender {
    NSInteger wave = [sender tag];
    NSInteger word = 26;
    vsu.waveram[wave][word] = computeWaveWord((int)[sender intValue]);
    [self updateUIFromWord:word forWave:wave];
}

- (IBAction)takeIntValueForWaveWord28From:(id)sender {
    NSInteger wave = [sender tag];
    NSInteger word = 27;
    vsu.waveram[wave][word] = computeWaveWord((int)[sender intValue]);
    [self updateUIFromWord:word forWave:wave];
}

- (IBAction)takeIntValueForWaveWord29From:(id)sender {
    NSInteger wave = [sender tag];
    NSInteger word = 28;
    vsu.waveram[wave][word] = computeWaveWord((int)[sender intValue]);
    [self updateUIFromWord:word forWave:wave];
}

- (IBAction)takeIntValueForWaveWord30From:(id)sender {
    NSInteger wave = [sender tag];
    NSInteger word = 29;
    vsu.waveram[wave][word] = computeWaveWord((int)[sender intValue]);
    [self updateUIFromWord:word forWave:wave];
}

- (IBAction)takeIntValueForWaveWord31From:(id)sender {
    NSInteger wave = [sender tag];
    NSInteger word = 30;
    vsu.waveram[wave][word] = computeWaveWord((int)[sender intValue]);
    [self updateUIFromWord:word forWave:wave];
}

- (IBAction)takeIntValueForWaveWord32From:(id)sender {
    NSInteger wave = [sender tag];
    NSInteger word = 31;
    vsu.waveram[wave][word] = computeWaveWord((int)[sender intValue]);
    [self updateUIFromWord:word forWave:wave];
}

- (IBAction)takeStateForEnabledFrom:(id)sender {
    NSInteger ch = [sender tag];
    vsu.sxint[ch] = computeIntReg([sender state] == NSOnState, isIntervalEnabled((int)vsu.sxint[ch]), intervalData((int)vsu.sxint[ch]));
    [self fillInIntRegFieldForChannel:ch];
}

- (IBAction)takeIntValueForXFrom:(id)sender {
    NSInteger ch = [sender tag];
    NSInteger x = [sender intValue];
    [self updateFreqForChannel:ch withX:x];
}

- (IBAction)takeFloatValueForHzFrom:(id)sender {
    NSInteger ch = [sender tag];
    NSInteger x;
    if (ch != 5)
    {
        x = computeXFromFreq([sender floatValue]);
    }
    else
    {
        x = computeXFromNoiseFreq([sender floatValue]);
    }
    [self updateFreqForChannel:ch withX:x];
}

- (IBAction)takeIntValueForWaveRAMFrom:(id)sender {
    NSInteger ch = [sender tag];
    vsu.sxram[ch] = computeWaveRAM((int)[sender indexOfSelectedItem]);
    [self fillInRAMRegFieldForChannel:ch];
}

- (IBAction)takeIntValueForTapLocationFrom:(id)sender {
    NSInteger ch = [sender tag];
    vsu.sxev1[ch] = computeEV1Reg(isEnvelopeEnabled(vsu.sxev1[ch]), isEnvelopeRepeating(vsu.sxev1[ch]), (int)[sender indexOfSelectedItem]);
    [self fillInEV1RegFieldForChannel:ch];
}

- (IBAction)takeStateForIntervalFrom:(id)sender {
    NSInteger ch = [sender tag];
    vsu.sxint[ch] = computeIntReg(isEnabled(vsu.sxint[ch]), [sender state] == NSOnState, intervalData(vsu.sxint[ch]));
    [self fillInIntRegFieldForChannel:ch];
}

- (IBAction)takeIntValueForIntervalDataFrom:(id)sender {
    NSInteger ch = [sender tag];
    vsu.sxint[ch] = computeIntReg(isEnabled(vsu.sxint[ch]), isIntervalEnabled(vsu.sxint[ch]), (int)[sender indexOfSelectedItem]);
    [self fillInIntRegFieldForChannel:ch];
}

- (IBAction)takeIntValueForLeftVolFrom:(id)sender {
    NSInteger ch = [sender tag];
    vsu.sxlrv[ch] = computeLRVReg((int)[sender intValue], rightVolume(vsu.sxlrv[ch]));
    [self fillInLRVRegFieldForChannel:ch];
}

- (IBAction)takeIntValueForRightVolFrom:(id)sender {
    NSInteger ch = [sender tag];
    vsu.sxlrv[ch] = computeLRVReg(leftVolume(vsu.sxlrv[ch]), (int)[sender intValue]);
    [self fillInLRVRegFieldForChannel:ch];
}

- (IBAction)takeStateForEnvelopeFrom:(id)sender {
    NSInteger ch = [sender tag];
    if (ch != 5)
    {
        vsu.sxev1[ch] = computeEV1Reg([sender state] == NSOnState, isEnvelopeRepeating(vsu.sxev1[ch]), isSweepEnabled(vsu.sxev1[ch]), isSweepRepeating(vsu.sxev1[ch]), modulationFunction(vsu.sxev1[ch]));
    }
    else
    {
        vsu.sxev1[ch] = computeEV1Reg([sender state] == NSOnState, isEnvelopeRepeating(vsu.sxev1[ch]), tapLocation(vsu.sxev1[ch]));
    }
    [self fillInEV1RegFieldForChannel:ch];
}

- (IBAction)takeStateForEnvRepeatFrom:(id)sender {
    NSInteger ch = [sender tag];
    if (ch != 5)
    {
        vsu.sxev1[ch] = computeEV1Reg(isEnvelopeEnabled(vsu.sxev1[ch]), [sender state] == NSOnState, isSweepEnabled(vsu.sxev1[ch]), isSweepRepeating(vsu.sxev1[ch]), modulationFunction(vsu.sxev1[ch]));
    }
    else
    {
        vsu.sxev1[ch] = computeEV1Reg(isEnvelopeEnabled(vsu.sxev1[ch]), [sender state] == NSOnState, tapLocation(vsu.sxev1[ch]));
    }
    [self fillInEV1RegFieldForChannel:ch];
}

- (IBAction)takeIntValueForEnvDirectionFrom:(id)sender {
    NSInteger ch = [sender tag];
    vsu.sxev0[ch] = computeEV0Reg(envInitialValue(vsu.sxev0[ch]), (int)[sender selectedRow], envStepTime(vsu.sxev0[ch]));
    [self fillInEV0RegFieldForChannel:ch];
}

- (IBAction)takeIntValueForEnvStepTimeFrom:(id)sender {
    NSInteger ch = [sender tag];
    vsu.sxev0[ch] = computeEV0Reg(envInitialValue(vsu.sxev0[ch]), envDirection(vsu.sxev0[ch]), (int)[sender indexOfSelectedItem]);
    [self fillInEV0RegFieldForChannel:ch];
}

- (IBAction)takeIntValueForEnvInitialValFrom:(id)sender {
    NSInteger ch = [sender tag];
    vsu.sxev0[ch] = computeEV0Reg((int)[sender intValue], envDirection(vsu.sxev0[ch]), envStepTime(vsu.sxev0[ch]));
    [self fillInEV0RegFieldForChannel:ch];
}

- (IBAction)takeStateForSweepFrom:(id)sender {
    NSInteger ch = [sender tag];
    vsu.sxev1[ch] = computeEV1Reg(isEnvelopeEnabled(vsu.sxev1[ch]), isEnvelopeRepeating(vsu.sxev1[ch]), [sender state] == NSOnState, isSweepRepeating(vsu.sxev1[ch]), modulationFunction(vsu.sxev1[ch]));
    [self fillInEV1RegFieldForChannel:ch];
}

- (IBAction)takeStateForSweepRepeatFrom:(id)sender {
    NSInteger ch = [sender tag];
    vsu.sxev1[ch] = computeEV1Reg(isEnvelopeEnabled(vsu.sxev1[ch]), isEnvelopeRepeating(vsu.sxev1[ch]), isSweepEnabled(vsu.sxev1[ch]), [sender state] == NSOnState, modulationFunction(vsu.sxev1[ch]));
    [self fillInEV1RegFieldForChannel:ch];
}

- (IBAction)takeIntValueForSweepFunctionFrom:(id)sender {
    NSInteger ch = [sender tag];
    vsu.sxev1[ch] = computeEV1Reg(isEnvelopeEnabled(vsu.sxev1[ch]), isEnvelopeRepeating(vsu.sxev1[ch]), isSweepEnabled(vsu.sxev1[ch]), isSweepRepeating(vsu.sxev1[ch]), (int)[sender selectedRow]);
    [self fillInEV1RegFieldForChannel:ch];
}

- (IBAction)takeIntValueForSweepDirectionFrom:(id)sender {
    NSInteger ch = [sender tag];
    if (ch == 4)
    {
        vsu.s5swp = computeSWPReg(sweepFreq(vsu.s5swp), sweepInterval(vsu.s5swp), (int)[sender selectedRow], sweepShift(vsu.s5swp));
        [self fillInSWPRegFieldForChannel:ch];
    }
}

- (IBAction)takeIntValueForSweepFreqFrom:(id)sender {
    NSInteger ch = [sender tag];
    if (ch == 4)
    {
        vsu.s5swp = computeSWPReg((int)[sender indexOfSelectedItem], sweepInterval(vsu.s5swp), sweepDirection(vsu.s5swp), sweepShift(vsu.s5swp));
        [self fillInSWPRegFieldForChannel:ch];
    }
}

- (IBAction)takeIntValueForSweepIntervalFrom:(id)sender {
    NSInteger ch = [sender tag];
    if (ch == 4)
    {
        vsu.s5swp = computeSWPReg(sweepFreq(vsu.s5swp), (int)[sender indexOfSelectedItem], sweepDirection(vsu.s5swp), sweepShift(vsu.s5swp));
        [self fillInSWPRegFieldForChannel:ch];
    }
}

- (IBAction)takeIntValueForSweepShiftFrom:(id)sender {
    NSInteger ch = [sender tag];
    if (ch == 4)
    {
        vsu.s5swp = computeSWPReg(sweepFreq(vsu.s5swp), sweepInterval(vsu.s5swp), sweepDirection(vsu.s5swp), (int)[sender intValue]);
        [self fillInSWPRegFieldForChannel:ch];
    }
}

#pragma mark Register fields

- (IBAction)takeIntValueForIntRegFrom:(id)sender {
    NSInteger ch = [sender tag];
    [[NSScanner scannerWithString:[sender stringValue]] scanHexInt:(unsigned int*)&vsu.sxint[ch]];
    [self updateUIFromIntRegForChannel:ch];
    [self fillInIntRegFieldForChannel:ch];
}

- (IBAction)takeIntValueForLRVRegFrom:(id)sender {
    NSInteger ch = [sender tag];
    [[NSScanner scannerWithString:[sender stringValue]] scanHexInt:(unsigned int*)&vsu.sxlrv[ch]];
    [self updateUIFromLRVRegForChannel:ch];
    [self fillInLRVRegFieldForChannel:ch];
}

- (IBAction)takeIntValueForFQHRegFrom:(id)sender {
    NSInteger ch = [sender tag];
    [[NSScanner scannerWithString:[sender stringValue]] scanHexInt:(unsigned int*)&vsu.sxfqh[ch]];
    [self fillInFreqFieldsForChannel:ch];
    [self fillInFQHRegFieldForChannel:ch];
}

- (IBAction)takeIntValueForFQLRegFrom:(id)sender {
    NSInteger ch = [sender tag];
    [[NSScanner scannerWithString:[sender stringValue]] scanHexInt:(unsigned int*)&vsu.sxfql[ch]];
    [self fillInFreqFieldsForChannel:ch];
    [self fillInFQLRegFieldForChannel:ch];
}

- (IBAction)takeIntValueForEV0RegFrom:(id)sender {
    NSInteger ch = [sender tag];
    [[NSScanner scannerWithString:[sender stringValue]] scanHexInt:(unsigned int*)&vsu.sxev0[ch]];
    [self updateUIFromEV0RegForChannel:ch];
    [self fillInEV0RegFieldForChannel:ch];
}

- (IBAction)takeIntValueForEV1RegFrom:(id)sender {
    NSInteger ch = [sender tag];
    [[NSScanner scannerWithString:[sender stringValue]] scanHexInt:(unsigned int*)&vsu.sxev1[ch]];
    [self updateUIFromEV1RegForChannel:ch];
    [self fillInEV1RegFieldForChannel:ch];
}

- (IBAction)takeIntValueForRAMRegFrom:(id)sender {
    NSInteger ch = [sender tag];
    if (ch < NUM_VSU_CHANNELS - 1)
    {
        [[NSScanner scannerWithString:[sender stringValue]] scanHexInt:(unsigned int*)&vsu.sxram[ch]];
        [self updateUIFromRAMRegForChannel:ch];
        [self fillInRAMRegFieldForChannel:ch];
    }
}

- (IBAction)takeIntValueForSWPRegFrom:(id)sender {
    NSInteger ch = [sender tag];
    if (ch == 4)
    {
        [[NSScanner scannerWithString:[sender stringValue]] scanHexInt:(unsigned int*)&vsu.s5swp];
        [self updateUIFromSWPRegForChannel:ch];
        [self fillInSWPRegFieldForChannel:ch];
    }
}

#pragma mark App housekeeping

- (void)dealloc
{
    [super dealloc];
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)theApplication
{
    return YES;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    for (int i = 0; i < 6; ++i)
    {
        for (int k = 0; k < 32; ++k)
        {
            vsu.waveram[i][k] = 0;
            [self updateUIFromWord:k forWave:i];
        }
    }
    for (int i = 0; i < NUM_VSU_CHANNELS; ++i)
    {
        vsu.sxint[i] = computeIntReg(false, false, 0);
        vsu.sxlrv[i] = computeLRVReg(0x0F, 0x0F);
        vsu.sxfqh[i] = computeFQHReg(0);
        vsu.sxfql[i] = computeFQLReg(0);
        vsu.sxev0[i] = computeEV0Reg(0x0F, 0, 0);
        vsu.sxev1[i] = computeEV1Reg(false, false);
        if (i < NUM_VSU_CHANNELS - 1)
        {
            vsu.sxram[i] = computeWaveRAM(0);
        }
        if (i == 4)
        {
            vsu.s5swp = 0x00;
        }
        [self fillInFreqFieldsForChannel:i];
        [self updateUIFromIntRegForChannel:i];
        [self fillInIntRegFieldForChannel:i];
        [self updateUIFromLRVRegForChannel:i];
        [self fillInLRVRegFieldForChannel:i];
        [self fillInFQHRegFieldForChannel:i];
        [self fillInFQLRegFieldForChannel:i];
        [self updateUIFromEV0RegForChannel:i];
        [self fillInEV0RegFieldForChannel:i];
        [self updateUIFromEV1RegForChannel:i];
        [self fillInEV1RegFieldForChannel:i];
        [self updateUIFromRAMRegForChannel:i];
        [self fillInRAMRegFieldForChannel:i];
        [self updateUIFromSWPRegForChannel:i];
        [self fillInSWPRegFieldForChannel:i];
    }
    
    self.audioEngine = [[BLTVSUAudioEngine alloc] init];
    [self.audioEngine start:vsu];
}
@end
